package com.chatlive.services;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.chatlive.commonutils.BundleKeys;
import com.chatlive.commonutils.Constants;
import com.chatlive.commonutils.JsonUtil;
import com.chatlive.callbacks.SocketConnectionListener;
import com.chatlive.callbacks.SocketEventCallback;
import com.chatlive.callbacks.SocketIOManager;
import com.chatlive.commonutils.SessionManager;
import com.chatlive.data.ChatMessage;
import com.chatlive.responsedata.UserData;
import com.chatlive.ui.GroupDetailActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.Exchanger;

/**
 * Created by admin on 30/01/2017.
 */

public class ChatService extends Service {

    private Looper mServiceLooper;
    private static ServiceHandler mServiceHandler;
    private HandlerThread thread;
    private JsonUtil jsonUtil;
    private SocketIOManager socketIOManager;

    private final int CONNECT_WITH_CHAT_SERVER = 1;
    private final static int DISCONNECT_WITH_CHAT_SERVER = 2;
    private final static int RETRY_CONNECTION = 3;
    private boolean isAttemptingPending = false;
    final public static String UPDATE_USER = "UPDATE_USER";

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case CONNECT_WITH_CHAT_SERVER: {
                    socketIOManager = SocketIOManager.getInstance();
                    System.out.println("diwanshu socket instantiated" + socketIOManager);
                    //connect chat server socket
                    socketIOManager.connect(Constants.URL.CHAT_SERVER_URL, new SocketConnectionListener() {
                        @Override
                        public void onConnect() {

                            System.out.println("diwanshu socket connected");
                            //attamp to send pending messages
                            /*if (!isAttemptingPending) {
                                attempPendingMessages();
                            }*/

                            socketIOManager.listenForEvent(SocketIOManager.MESSAGE_TYPE.USERLIST, new SocketEventCallback() {
                                @Override
                                public void onEventOccur(JSONObject obj) {
                                    Log.e("diwanshu user list", "USERLIST");
                                    ArrayList<String> mUserList = new ArrayList<String>();
                                    /*handlePing(jsonUtil.fromJson(obj.toString(), ChatMessage.class));*/
                                    try {
                                        if (obj.has("online_user")) {
                                            JSONArray jArray = obj.getJSONArray("online_user");
                                            for (int i = 0; i < jArray.length(); i++) {
                                                mUserList.add(jArray.getString(i));
                                            }
                                            Intent intent = new Intent();
                                            intent.setAction(UPDATE_USER);
                                            Bundle messageBundle = new Bundle();
                                            messageBundle.putSerializable(BundleKeys.UPDATEUSER_BUNDLE, mUserList);
                                            intent.putExtras(messageBundle);
                                            sendBroadcast(intent);
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                }


                            });

                            socketIOManager.listenForEvent(SocketIOManager.MESSAGE_TYPE.CONNECTED, new SocketEventCallback() {
                                @Override
                                public void onEventOccur(JSONObject obj) {
                                    System.out.println("diwanshu connected");
                                }
                            });

                            //ping listener
                            socketIOManager.listenForEvent(SocketIOManager.MESSAGE_TYPE.PING, new SocketEventCallback() {
                                @Override
                                public void onEventOccur(JSONObject obj) {
                                    Log.e("diwanshu type ping", "\n" + obj.toString());
                                    UserData userData = SessionManager.getSavedObjectFromPreference(getApplication(),
                                            Constants.SHAREDPREFRENCE,
                                            "userData", UserData.class);
                                    try {
                                        if (String.valueOf(userData.getUserId()).equalsIgnoreCase(obj.getString("from_user_id"))) {
                                            Log.e("diwanshu type ping", "duplicate message");
                                        } else {
                                            handlePing(jsonUtil.fromJson(obj.toString(), ChatMessage.class));
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                            //ping pass ack
                            socketIOManager.listenForEvent(SocketIOManager.MESSAGE_TYPE.PINGPASS, new SocketEventCallback() {
                                @Override
                                public void onEventOccur(JSONObject obj) {
                                    Log.e("objecxt", "" + obj.toString());
                                    int chatId = 0;
                                    try {
                                        chatId = obj.getInt("destination_id");

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    sendBroadCastMessage(jsonUtil.fromJson(obj.toString(), ChatMessage.class), SocketIOManager.MESSAGE_TYPE.PINGPASS);
                                }
                            });
                            //ping type start
                            socketIOManager.listenForEvent(SocketIOManager.MESSAGE_TYPE.TYPE_START, new SocketEventCallback() {
                                @Override
                                public void onEventOccur(JSONObject obj) {
                                    System.out.println("diwanshu typing start");
                                    sendBroadCastMessage(jsonUtil.fromJson(obj.toString(), ChatMessage.class), SocketIOManager.MESSAGE_TYPE.TYPE_START);
                                }
                            });

                            //ping type end
                            socketIOManager.listenForEvent(SocketIOManager.MESSAGE_TYPE.TYPE_END, new SocketEventCallback() {
                                @Override
                                public void onEventOccur(JSONObject obj) {
                                    System.out.println("diwanshu typing end");
                                    sendBroadCastMessage(jsonUtil.fromJson(obj.toString(), ChatMessage.class), SocketIOManager.MESSAGE_TYPE.TYPE_END);
                                }
                            });
                            //ping FAIL listener
                            socketIOManager.listenForEvent(SocketIOManager.MESSAGE_TYPE.PINGFAIL, new SocketEventCallback() {
                                @Override
                                public void onEventOccur(JSONObject obj) {
                                    System.out.println("diwanshu PING FAIL");
                                    sendBroadCastMessage(jsonUtil.fromJson(obj.toString(), ChatMessage.class), SocketIOManager.MESSAGE_TYPE.PINGFAIL);
                                }
                            });
                            socketIOManager.listenForEvent(SocketIOManager.MESSAGE_TYPE.ADDUSER, new SocketEventCallback() {
                                @Override
                                public void onEventOccur(JSONObject obj) {
                                    sendBroadCastMessage(jsonUtil.fromJson(obj.toString(), ChatMessage.class), SocketIOManager.MESSAGE_TYPE.ADDUSER);
                                }
                            });
                            socketIOManager.listenForEvent(SocketIOManager.MESSAGE_TYPE.LEFTUSER, new SocketEventCallback() {
                                @Override
                                public void onEventOccur(JSONObject obj) {
                                    UserData userData = SessionManager.getSavedObjectFromPreference(getApplication(),
                                            Constants.SHAREDPREFRENCE,
                                            "userData", UserData.class);

                                    try {
                                        if (obj.getString("user_id").equalsIgnoreCase(String.valueOf(userData.getUserId()))) {
                                            sendBroadCastMessage(null, SocketIOManager.MESSAGE_TYPE.RECONNECT);
                                        } else {
                                            sendBroadCastMessage(jsonUtil.fromJson(obj.toString(), ChatMessage.class), SocketIOManager.MESSAGE_TYPE.LEFTUSER);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                }
                            });

                        }

                        @Override
                        public void onDisconnect() {
                            socketIOManager.destroyEvent(SocketIOManager.MESSAGE_TYPE.PING);
                            socketIOManager.destroyEvent(SocketIOManager.MESSAGE_TYPE.PINGPASS);
                            socketIOManager.destroyEvent(SocketIOManager.MESSAGE_TYPE.TYPE_START);
                            socketIOManager.destroyEvent(SocketIOManager.MESSAGE_TYPE.TYPE_END);
                            socketIOManager.destroyEvent(SocketIOManager.MESSAGE_TYPE.ADDUSER);
                            socketIOManager.destroyEvent(SocketIOManager.MESSAGE_TYPE.LEFTUSER);
                            sendBroadCastMessage(null, SocketIOManager.MESSAGE_TYPE.DISCONNECT);
                        }

                        @Override
                        public void onReconnect() {
                            sendBroadCastMessage(null, SocketIOManager.MESSAGE_TYPE.RECONNECT);
                        }
                    });
                    break;
                }
            }
        }
    }


    /*private void attempPendingMessages() {
        List<ChatMessage> messages = chatDataSource.getUnsentChats(0);   // Set userId

        Iterator<ChatMessage> iterator = messages.iterator();

        while (iterator.hasNext()) {
            isAttemptingPending = true;
            ChatMessage message = iterator.next();
            message.setIsRead(false);
            socketIOManager.emit(SocketIOManager.MESSAGE_TYPE.PING, jsonUtil.toJson(message));

            if (!iterator.hasNext()) {
                isAttemptingPending = false;
            }
        }

    }*/

    private void handlePing(ChatMessage chatMessage) {
        sendBroadCastMessage(chatMessage, SocketIOManager.MESSAGE_TYPE.PING);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Message msg = mServiceHandler.obtainMessage();
        msg.what = CONNECT_WITH_CHAT_SERVER;
        mServiceHandler.sendMessage(msg);
        return START_STICKY;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        socketIOManager.destroyEvent(SocketIOManager.MESSAGE_TYPE.PING);
        socketIOManager.destroyEvent(SocketIOManager.MESSAGE_TYPE.PINGPASS);
        socketIOManager.destroyEvent(SocketIOManager.MESSAGE_TYPE.TYPE_START);
        socketIOManager.destroyEvent(SocketIOManager.MESSAGE_TYPE.TYPE_END);
        socketIOManager.destroyEvent(SocketIOManager.MESSAGE_TYPE.ADDUSER);
        socketIOManager.destroyEvent(SocketIOManager.MESSAGE_TYPE.LEFTUSER);
        socketIOManager.destroyEvent(SocketIOManager.MESSAGE_TYPE.RECONNECT);
        socketIOManager.destroyEvent(SocketIOManager.MESSAGE_TYPE.DISCONNECT);
        socketIOManager.disconnect();
        /*chatDataSource.close();*/
    }

    @Override
    public void onCreate() {
        super.onCreate();
        thread = new HandlerThread("Uchange Chat Service Thread",
                android.os.Process.THREAD_PRIORITY_BACKGROUND);
        if (!thread.isAlive())
            thread.start();
        mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);
        jsonUtil = JsonUtil.getInstance();
        /*prefManager = new SharedPreferenceManager(this, Constants.PREF_NAME);*/
    }

    private void sendBroadCastMessage(ChatMessage message, String messageType) {
        Intent intent = new Intent();
        intent.setAction(Constants.PING_CONSTANTS.PING_ACTION);
        Bundle messageBundle = new Bundle();
        messageBundle.putSerializable(BundleKeys.PING_BUNDLE, message);
        messageBundle.putString(BundleKeys.PING_TYPE_BUNDLE, messageType);
        intent.putExtras(messageBundle);
        sendBroadcast(intent);
    }


}
