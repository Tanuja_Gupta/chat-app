package com.chatlive.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.chatlive.R;
import com.chatlive.commonutils.Constants;
import com.chatlive.commonutils.JsonUtil;
import com.chatlive.commonutils.SessionManager;
import com.chatlive.data.ChatMessage;
import com.chatlive.database.DatabaseHandler;
import com.chatlive.responsedata.UserData;
import com.chatlive.ui.ChatActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Intex pc on 2/6/2017.
 */

public class MessageingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]
        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.e(TAG, "From: " + remoteMessage.getFrom());
        ChatMessage message = null;
        DatabaseHandler db = new DatabaseHandler(this);
        UserData userData = SessionManager.getSavedObjectFromPreference(this,
                Constants.SHAREDPREFRENCE,
                "userData", UserData.class);
        int mute;
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Message data payload: " + remoteMessage.getData().get("ping"));
            String data = remoteMessage.getData().get("ping");
            try {
                JsonUtil jsonUtil = JsonUtil.getInstance();
                JSONObject jsonObject = new JSONObject(data);
                if (jsonObject.has("userList")) {
                    int fromUserId = 0;
                    if (jsonObject.get("userList") instanceof JSONArray) {
                        JSONArray jarr = jsonObject.getJSONArray("userList");
                        for (int i = 0; i < jarr.length(); i++) {
                            if (Integer.parseInt(jarr.getJSONObject(i).getString("userId")) == userData.getUserId()) {
                                fromUserId = Integer.parseInt(jarr.getJSONObject(i).getString("userId"));
                                break;
                            }
                        }
                    } else {
                        fromUserId = Integer.parseInt(jsonObject.getString("userList"));

                    }
                    message = new ChatMessage();
                    message.setFromUserId(fromUserId);
                    message.setGroupName("group_name");
                    message.setMessageType(Integer.parseInt(jsonObject.getString("messageType")));
                    message.setCreatedOn(jsonObject.getLong("created_on"));
                    message.setToUserId(jsonObject.getLong("destination_id"));
                    message.setDestination_type(Integer.parseInt(jsonObject.getString("destination_type")));
                    message.setMute(0);
                    message.setMessage(jsonObject.getString("message"));
                    if (db.chatExist(message.getToUserId()) != 0) {
                        int chatId = db.chatExist(message.getToUserId());
                        db.addMessage(chatId, message);
                    } else {
                        db.addRecentChatUser(message.getToUserId(), "", message.getDestination_type(), 0);
                        int chatId = db.chatExist(jsonObject.getInt("destination_id"));
                        db.addMessage(chatId, message);
                    }
                } else {
                    message = jsonUtil.fromJson(jsonObject.toString(), ChatMessage.class);
                    //unread message value 0
                    message.setRead_status(0);
                    int chatId;
                    if (userData.getUserId() == jsonObject.getInt("destination_id")) {
                        if (db.chatExist(jsonObject.getInt("from_user_id")) != 0) {
                            chatId = db.chatExist(jsonObject.getInt("from_user_id"));
                            mute = db.getMuteStatus(chatId);
                            message.setMute(mute);
                            db.addMessage(chatId, message);
                        } else {
                            db.addRecentChatUser(jsonObject.getInt("from_user_id"), "", jsonObject.getInt("destination_type"), 0);
                            chatId = db.chatExist(jsonObject.getInt("from_user_id"));
                            mute = db.getMuteStatus(chatId);
                            message.setMute(mute);
                            db.addMessage(chatId, message);

                        }

                    } else {
                        if (db.chatExist(jsonObject.getInt("destination_id")) != 0) {
                            chatId = db.chatExist(jsonObject.getInt("destination_id"));
                            mute = db.getMuteStatus(chatId);
                            message.setMute(mute);
                            db.addMessage(chatId, message);
                        } else {
                            db.addRecentChatUser(jsonObject.getInt("destination_id"), "", jsonObject.getInt("destination_type"), 0);
                            chatId = db.chatExist(jsonObject.getInt("destination_id"));
                            mute = db.getMuteStatus(chatId);
                            message.setMute(mute);
                            mute = db.getMuteStatus(chatId);
                        }
                    }
                    if (mute == 0) {
                        sendNotification(chatId, jsonObject.getInt("destination_type"), jsonObject.getString("from_user_id"));
                    }


                }


            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Message Notification Body:--- " + remoteMessage.getNotification().getBody());

        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received FCM message.
     */
    private void sendNotification(int chatId, int destinationtype, String from_user_id) {


        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra(Constants.DESTINATIONID, from_user_id);
        intent.putExtra("phoneNumber", "");
        intent.putExtra("destinationtype", destinationtype);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);
        DatabaseHandler db = new DatabaseHandler(this);
         //Fetching all unread messages from Local Db
        List<ChatMessage> unreadchat = db.getAllUnreadChat(chatId, 0);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Chat Live")
                .setContentText(unreadchat.size() + " unread message")
                .setStyle(getExpandedNotificationStyle(unreadchat))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


        notificationManager.notify(Integer.parseInt(from_user_id) /* ID of notification */, notificationBuilder.build());
    }

    private NotificationCompat.Style getExpandedNotificationStyle(List<ChatMessage> unreadchat) {
        NotificationCompat.InboxStyle expandedNotificationStyle = new NotificationCompat.InboxStyle();
//        expandetdNotificationStyle.setBigContentTitle("Expanded Content Title");
        // There seems to be a bug in the notification display system where, unless you set
        // summary text, single line expanded inbox state will not expand when the notif
        // drawer is fully pulled down. However, it still works in the lock-screen.
        //   expandedNoificationStyle.setSummaryText("Live Chat" + unreadchat.size() + " unread messages");
        for (int i = 0; i < unreadchat.size(); i++) {
            expandedNotificationStyle.addLine(unreadchat.get(i).getFromUserId() + " : " + unreadchat.get(i).getMessage());
        }
        return expandedNotificationStyle;
    }
}