package com.chatlive.services;

import com.chatlive.responsedata.AddGroupRequest;
import com.chatlive.responsedata.AddGroupResponse;
import com.chatlive.responsedata.AddGroupUserRequest;
import com.chatlive.responsedata.ChatFileResponse;
import com.chatlive.responsedata.DeleteGroup;
import com.chatlive.responsedata.DeleteGroupResponse;
import com.chatlive.responsedata.GroupDetail;
import com.chatlive.responsedata.GroupDetailRequest;
import com.chatlive.responsedata.GroupDetailsResponse;
import com.chatlive.responsedata.GroupUser;
import com.chatlive.responsedata.OTPresquest;
import com.chatlive.responsedata.RegisterRequest;
import com.chatlive.responsedata.SyncContactRequest;
import com.chatlive.responsedata.UpdateUserResponse;
import com.chatlive.responsedata.UserListResponse;
import com.chatlive.responsedata.UserResponse;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Intex pc on 2/3/2017.
 */

public interface ApiCallService {
    @POST("register")
    Call<UserResponse> registerUser(@Body RegisterRequest request);

    @POST("verify_user")
    Call<UserResponse> userOtp(@Body OTPresquest request);

    @Multipart
    @POST("update_profile")
    Call<UserResponse> updateUser(
            @Part("userId") RequestBody userId,
            @Part("profileName") RequestBody profileName,
            @Part("profileImage") RequestBody profileImage,
            @Part MultipartBody.Part avatarImage);

    @GET("get_users")
    Call<UserListResponse> getUserList(@Query("userId") String userId);

    @Multipart
    @POST("upload_file")
    Call<ChatFileResponse> uploadFile(
            @Part("userId") RequestBody userId,
            @Part("messageType") RequestBody messageType,
            @Part MultipartBody.Part chatImage,
            @Part MultipartBody.Part chatVideo);

    @POST("chat_group/create_group")
    Call<AddGroupResponse> addGroup(@Body AddGroupRequest request, @Header("Authorization") String  authHeader);
    @POST("chat_group/add_group_user")
    Call<AddGroupResponse> addUserGroup(@Body AddGroupUserRequest userRequest, @Header("Authorization") String  authHeader);
    @POST("chat_group/delete_user")
    Call<DeleteGroupResponse> deleteGroupUser(@Body DeleteGroup request, @Header("Authorization") String  authHeader);
    @POST("chat_group/group_users")
    Call<GroupDetailsResponse> getGroupDetails(@Body GroupDetailRequest request, @Header("Authorization") String  authHeader);

    @POST("chat_group/delete_group")
    Call<DeleteGroupResponse> deleteGroup(@Body DeleteGroup request, @Header("Authorization") String  authHeader);
    @FormUrlEncoded
    @POST("a/sync_contacts")
    Call<UserListResponse> synContact(@Field ("contact_array") ArrayList<String> contact_array,@Field ("code_array") ArrayList<String> code_array, @Header("Authorization") String  authHeader);
}