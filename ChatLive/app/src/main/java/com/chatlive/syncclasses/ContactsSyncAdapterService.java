package com.chatlive.syncclasses;

import android.accounts.Account;
import android.accounts.OperationCanceledException;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.OperationApplicationException;
import android.content.SyncResult;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.provider.BaseColumns;
import android.provider.ContactsContract;
import android.util.Log;
import android.widget.Toast;

import com.chatlive.ChatLiveApplication;
import com.chatlive.R;
import com.chatlive.callbacks.SocketIOManager;
import com.chatlive.commonutils.CommonUtils;
import com.chatlive.commonutils.Constants;
import com.chatlive.commonutils.SessionManager;
import com.chatlive.data.MyContact;
import com.chatlive.responsedata.AddGroupResponse;
import com.chatlive.responsedata.AddGroupUserRequest;
import com.chatlive.responsedata.SyncContactRequest;
import com.chatlive.responsedata.UserData;
import com.chatlive.responsedata.UserListResponse;
import com.chatlive.services.ChatService;
import com.chatlive.ui.AddGroupActivity;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Intex pc on 2/10/2017.
 */

public class ContactsSyncAdapterService extends Service {
    private static final String TAG = "ContactsSyncAdapterService";
    private static SyncAdapterImpl sSyncAdapter = null;
    private static ContentResolver mContentResolver = null;
    private static String UsernameColumn = ContactsContract.RawContacts.SYNC1;
    private static String PhotoTimestampColumn = ContactsContract.RawContacts.SYNC2;
    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<String> mIDs = new ArrayList<>();
    private ArrayList<String> mNumbers = new ArrayList<>();
    private static String MIMETYPE = "vnd.android.cursor.item/com.chatlive";

    public ContactsSyncAdapterService() {
        super();
    }

    private class SyncAdapterImpl extends AbstractThreadedSyncAdapter {
        private Context mContext;

        public SyncAdapterImpl(Context context) {
            super(context, true);
            mContext = context;
        }

        @Override
        public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {

            Log.e("Force syn", "called");
            if (mContext != null) {
                //  Toast.makeText(mContext, "sync calld", Toast.LENGTH_SHORT).show();
            }
            try {
                performSync(mContext, account, extras, authority, provider, syncResult);
            } catch (OperationCanceledException e) {
                e.printStackTrace();

                Log.e("Sync", "" + e.getMessage());
            }

        }
    }

    @Override
    public void onCreate() {
        super.onCreate();

    }

    @Override
    public IBinder onBind(Intent intent) {
        IBinder ret = null;
        ret = getSyncAdapter().getSyncAdapterBinder();
        return ret;
    }

    private SyncAdapterImpl getSyncAdapter() {
        if (sSyncAdapter == null)
            sSyncAdapter = new SyncAdapterImpl(this);
        return sSyncAdapter;
    }

    private void performSync(Context context, Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult)
            throws OperationCanceledException {
        //      Retrieve names from phone's contact list and save in mNames
        getContactDataBefore();
        String name, number = null, id;
        for (int i = 0; i < mIDs.size(); i++) {
            id = mIDs.get(i);
            number = mNumbers.get(i);
            addContact(context, new MyContact(id, number), account);
        }


        UserData userData = SessionManager.getSavedObjectFromPreference(context,
                Constants.SHAREDPREFRENCE,
                Constants.USERDATA, UserData.class);
        getContactDataAfter(userData.getISO(), userData.getAuthToken());

    }

    /**
     * Method to fetch contact's from device
     */
    private void getContactDataBefore() {
        int i = 0;
        // query all contact id's from device
        Cursor c1 = getContentResolver().query(ContactsContract.Contacts.CONTENT_URI,
                new String[]{ContactsContract.Contacts._ID}, null, null, null);

        if ((c1 != null) && c1.moveToFirst()) {

            // add contact id's to the mIDs list
            do {
                mIDs.add(c1.getString(c1.getColumnIndexOrThrow(ContactsContract.Contacts._ID)));

                // query all contact numbers corresponding to current id
                Cursor c2 = getContentResolver()
                        .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER},
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?",
                                new String[]{mIDs.get(i)}, null);

                if (c2 != null && c2.moveToFirst()) {
                    // add contact number's to the mNumbers list
                    do {
                        mNumbers.add(c2.getString(c2
                                .getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER)));
                    } while (c2.moveToNext());
                    c2.close();
                }

                i++;
            } while (c1.moveToNext() && i < c1.getCount());

            c1.close();
        }
        // Log.e("Number ", mNumbers.size() + "..... " + mIDs.size());
    }

    public void addContact(Context context, MyContact contact, Account account) {

        ContentResolver resolver = context.getContentResolver();
        boolean mHasAccount = isAlreadyRegistered(resolver, contact.Id);

        if (mHasAccount) {
            Log.e("Contact Manager", context.getString(R.string.already_exist));
        } else {

            ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();

            // insert account name and account type
            ops.add(ContentProviderOperation
                    .newInsert(addCallerIsSyncAdapterParameter(ContactsContract.RawContacts.CONTENT_URI, true))
                    .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, account.name)
                    .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, account.type)
                    .withValue(ContactsContract.RawContacts.AGGREGATION_MODE,
                            ContactsContract.RawContacts.AGGREGATION_MODE_DEFAULT)
                    .build());

            // insert contact number
            ops.add(ContentProviderOperation
                    .newInsert(addCallerIsSyncAdapterParameter(ContactsContract.Data.CONTENT_URI, true))
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, contact.number)
                    .build());

            // insert contact name
//            ops.add(ContentProviderOperation
//                    .newInsert(addCallerIsSyncAdapterParameter(ContactsContract.Data.CONTENT_URI, true))
//                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
//                    .withValue(ContactsContract.Data.MIMETYPE,
//                            ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
//                    .withValue(CommonDataKinds.StructuredName.DISPLAY_NAME, contact.name)
//                    .build());

            // insert mime-type data
            ops.add(ContentProviderOperation
                    .newInsert(addCallerIsSyncAdapterParameter(ContactsContract.Data.CONTENT_URI, true))
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(ContactsContract.Data.MIMETYPE, MIMETYPE)
                    .withValue(ContactsContract.Data.DATA1, 12345)
                    .withValue(ContactsContract.Data.DATA2, "user")
                    .withValue(ContactsContract.Data.DATA3, "myContact")
                    .build());

            try {
                resolver.applyBatch(ContactsContract.AUTHORITY, ops);
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("Sync add ", "" + e.getMessage());
            }

        }
    }

    /**
     * Check if contact is already registered with app
     *
     * @param resolver
     * @param id
     * @return
     */
    private static boolean isAlreadyRegistered(ContentResolver resolver, String id) {

        boolean isRegistered = false;
        List<String> str = new ArrayList<>();

        //query raw contact id's from the contact id
        Cursor c = resolver.query(ContactsContract.RawContacts.CONTENT_URI, new String[]{ContactsContract.RawContacts._ID},
                ContactsContract.RawContacts.CONTACT_ID + "=?",
                new String[]{id}, null);

        //fetch all raw contact id's and save them in a list of string
        if (c != null && c.moveToFirst()) {
            do {
                str.add(c.getString(c.getColumnIndexOrThrow(ContactsContract.RawContacts._ID)));
            } while (c.moveToNext());
            c.close();
        }

        //query account types and check the account type for each raw contact id
        for (int i = 0; i < str.size(); i++) {
            Cursor c1 = resolver.query(ContactsContract.RawContacts.CONTENT_URI, new String[]{ContactsContract.RawContacts.ACCOUNT_TYPE},
                    ContactsContract.RawContacts._ID + "=?",
                    new String[]{str.get(i)}, null);

            if (c1 != null) {
                c1.moveToFirst();
                String accType = c1.getString(c1.getColumnIndexOrThrow(ContactsContract.RawContacts.ACCOUNT_TYPE));
                if (accType != null && accType.equals("com.chatlive")) {
                    isRegistered = true;
                    break;
                }
                c1.close();
            }
        }

        return isRegistered;
    }

    /**
     * Check for sync call
     *
     * @param uri
     * @param isSyncOperation
     * @return
     */
    private static Uri addCallerIsSyncAdapterParameter(Uri uri, boolean isSyncOperation) {
        if (isSyncOperation) {
            return uri.buildUpon()
                    .appendQueryParameter(ContactsContract.CALLER_IS_SYNCADAPTER, "true")
                    .build();
        }
        return uri;
    }

    /**
     * Method to fetch contacts after updation (for logging purposes)
     */
    private ArrayList<String> getContactDataAfter(String ISO, String authToken) {
        Cursor c = getContentResolver()
                .query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);

        List<String> RIds = new ArrayList<>();
        mIDs = new ArrayList<>();
        mNumbers = new ArrayList<>();
        int i = 0;
        ArrayList<String> contact_list = new ArrayList<>();
        ArrayList<String> code_list = new ArrayList<>();
        JSONArray jsonArray = new JSONArray();
        if (c != null && c.moveToFirst()) {
            do {
                mIDs.add(c.getString(c
                        .getColumnIndexOrThrow(ContactsContract.Contacts._ID)));
                mNames.add(c.getString(c
                        .getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME)));

                Cursor c2 = getContentResolver()
                        .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER},
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?",
                                new String[]{mIDs.get(i)}, null);

                if (c2 != null && c2.moveToFirst()) {
                    do {
                        mNumbers.add(c2.getString(c2
                                .getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER)));
                    } while (c2.moveToNext());
                    c2.close();
                }

                Cursor rawcontacts = getContentResolver()
                        .query(ContactsContract.RawContacts.CONTENT_URI,
                                new String[]{ContactsContract.RawContacts._ID},
                                ContactsContract.RawContacts.CONTACT_ID + "=?",
                                new String[]{mIDs.get(i)}, null);

                if (rawcontacts != null && rawcontacts.moveToFirst()) {
                    do {
                        RIds.add(rawcontacts.getString(rawcontacts
                                .getColumnIndexOrThrow(ContactsContract.RawContacts._ID)));
                    } while (rawcontacts.moveToNext());
                    rawcontacts.close();
                }
//                "Bearer " + userData.getAuthToken(


                JSONObject jsonObject = new JSONObject();
                try {

                    try {
                        // phone must begin with '+'
                        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
                        Phonenumber.PhoneNumber numberProto = phoneUtil.parse(mNumbers.get(i), ISO);
                        int countryCode = numberProto.getCountryCode();
                        String nationalNumber = String.valueOf(numberProto.getNationalNumber());
//                        jsonObject.put("phone_number", nationalNumber);
//                        jsonObject.put("code", countryCode);
//                        jsonArray.put(jsonObject);
                        code_list.add(String.valueOf(countryCode));
                        contact_list.add(nationalNumber);
                        Log.i("code", "code " + countryCode);
                        Log.i("code", "national number " + nationalNumber);

                    } catch (NumberParseException e) {
                        System.err.println("NumberParseException was thrown: " + e.toString());
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.e("Contact after Sync  ", jsonArray.toString());
                Log.e("Contact after Sync  ", mNumbers.get(i));
                i++;
            } while (c.moveToNext());
            c.close();
        }
        Log.e(TAG, "Adding contact: " + contact_list);
        if (contact_list.size() > 0) {
            syncContacts(authToken, contact_list, code_list);
        }
        return contact_list;
    }


    public void syncContacts(String auth, final ArrayList<String> jsonArray, final ArrayList<String> code_list) {
        // adding another part within the multipart request for rest of fields

        if (CommonUtils.checkInternetConnection(this)) {
//            SyncContactRequest request = new SyncContactRequest(jsonArray);
            Call<UserListResponse> wsOTP = ((ChatLiveApplication) getApplicationContext())
                    .mApiCall.synContact(jsonArray, code_list, "Bearer " + auth);
            wsOTP.enqueue(new Callback<UserListResponse>() {
                @Override
                public void onResponse(Call<UserListResponse> call, Response<UserListResponse> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus().equalsIgnoreCase(Constants.SUCCESS)) {


                            Log.e("RESULT", response.body().getData().size() + "");
                            for (int i = 0; i < response.body().getData().size(); i++) {
                                Log.e("Data", response.body().getData().get(i).getPhoneNumber());
                                Log.e("Data", response.body().getData().get(i).getUserId());
                            }
                            Log.e("RESULT", "Succes");

                        }


                    } else {
                        Log.e("RESULT", "Error");
                    }
                }

                @Override
                public void onFailure(Call<UserListResponse> call, Throwable t) {
                    t.printStackTrace();
                    Log.e("RESULT", "Exception");
                }
            });
        }
    }
}

