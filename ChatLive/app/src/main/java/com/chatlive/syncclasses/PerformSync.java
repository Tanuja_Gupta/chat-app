package com.chatlive.syncclasses;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.chatlive.R;

/**
 * Created by Intex pc on 3/1/2017.
 */

public class PerformSync {
    Context ctx;

    public PerformSync(Context ctx) {
        this.ctx = ctx;
    }

    public void startForceSync() {
        Bundle bundle = new Bundle();
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        Account account = new Account(getCurrentAccount(), ctx.getString(R.string.ACCOUNT_TYPE));
        ContentResolver.requestSync(account, ctx.getString(R.string.AUTHORITY), bundle);
        ContentResolver.setIsSyncable(account, ctx.getString(R.string.AUTHORITY), 1);
        ContentResolver.setSyncAutomatically(account, ctx.getString(R.string.AUTHORITY), true);
        Log.e("Account", "@@@" + account.name);
    }

    private String getCurrentAccount() {
        Account[] accounts = AccountManager.get(ctx).getAccountsByType(ctx.getString(R.string.ACCOUNT_TYPE));
        return accounts.length < 0 ? "" : accounts[0].name;
    }
}
