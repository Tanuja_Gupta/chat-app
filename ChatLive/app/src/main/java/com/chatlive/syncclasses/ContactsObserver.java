package com.chatlive.syncclasses;

import android.accounts.Account;
import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.chatlive.R;
import com.chatlive.ui.MainActivity;

/**
 * Created by Intex pc on 2/28/2017.
 */

public class ContactsObserver extends ContentObserver {
    Context context;

    public ContactsObserver(Context context) {

        super(null);
        this.context = context;
        Log.e("Called", "initialized");
    }

    @Override
    public void onChange(boolean selfChange) {
        this.onChange(selfChange, null);
    }

    @Override
    public void onChange(boolean selfChange, Uri uri) {
        // do s.th.
        // depending on the handler you might be on the UI
        // thread, so be cautious!
        PerformSync prSyn = new PerformSync(context);
        prSyn.startForceSync();
        Log.e("Called", "Content observer Called");
    }


}
