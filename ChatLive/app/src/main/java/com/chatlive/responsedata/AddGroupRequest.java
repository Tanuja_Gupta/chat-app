package com.chatlive.responsedata;

/**
 * Created by Intex pc on 2/9/2017.
 */

public class AddGroupRequest {
    private String group_name;
    private int user_id;

    public AddGroupRequest(String group_name, int user_id) {
        this.group_name = group_name;
        this.user_id = user_id;
    }

    public int getUserId() {
        return user_id;
    }

    public String getGroupName() {
        return group_name;
    }
}
