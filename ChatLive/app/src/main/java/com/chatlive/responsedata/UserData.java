package com.chatlive.responsedata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Intex pc on 2/3/2017.
 */


public class UserData {
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("phone_number")
    @Expose
    private String phoneNumber;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("avatar_image")
    @Expose
    private String avatarImage;
    @SerializedName("auth_token")
    @Expose
    private String authToken;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("is_verified")
    @Expose
    private Integer isVerified;
    @SerializedName("iso")
    @Expose
    private String iso;

    public String getISO() {
        return iso;
    }

    public void setISO(String ISO) {
        this.iso = iso;
    }


    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatarImage() {
        return avatarImage;
    }

    public void setAvatarImage(String avatarImage) {
        this.avatarImage = avatarImage;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getIsVerified() {
        return isVerified;
    }

    public void setIsVerified(Integer isVerified) {
        this.isVerified = isVerified;
    }
}