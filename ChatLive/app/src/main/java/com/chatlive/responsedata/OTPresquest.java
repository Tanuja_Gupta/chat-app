package com.chatlive.responsedata;

/**
 * Created by Intex pc on 2/3/2017.
 */

public class OTPresquest {
    private int userId;
    private String otp;
    private String gcmId;
    private String deviceToken;
    private String deviceId;

    public OTPresquest(int userId, String otp, String gcmId, String deviceToken, String deviceId) {
        this.userId = userId;
        this.otp = otp;
        this.gcmId=gcmId;
        this.deviceId=deviceId;
        this. deviceToken=deviceToken;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public int getUserID() {
        return userId;
    }

    public void setUserID(int userId) {
        this.userId = userId;
    }
}
