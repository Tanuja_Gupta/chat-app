package com.chatlive.responsedata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Intex pc on 2/14/2017.
 */

public class UserList {
    @SerializedName("user_detail")
    @Expose
    private List<ChatUserData> userDetail = null;

    @SerializedName("group_list")
    @Expose
    private List<ChatGroupData> groupList = null;

    public List<ChatUserData> getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(List<ChatUserData> userDetail) {
        this.userDetail = userDetail;
    }

    public List<ChatGroupData> getGroupList() {
        return groupList;
    }

    public void setGroupList(List<ChatGroupData> groupList) {
        this.groupList = groupList;
    }
}
