package com.chatlive.responsedata;

import java.io.File;

import okhttp3.MultipartBody;
import retrofit2.http.Part;

/**
 * Created by Intex pc on 2/3/2017.
 */

public class UpdateUserRequest {
    private File avatarImage;
    private String userId;
    private String profileName;
    private String profileImage;




    public UpdateUserRequest(MultipartBody multipartBody) {
        this.avatarImage = avatarImage;
        this.userId = userId;
        this.profileImage = profileImage;
        this.profileName = profileName;

    }

    public File getAvatarImage() {
        return avatarImage;
    }

    public void setAvatarImage(File avatarImage) {
        this.avatarImage = avatarImage;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public String getProfileName() {
        return profileName;
    }
}
