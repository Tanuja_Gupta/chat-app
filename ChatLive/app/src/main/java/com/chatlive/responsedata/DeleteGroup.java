package com.chatlive.responsedata;

import org.json.JSONArray;

/**
 * Created by Intex pc on 2/15/2017.
 */

public class DeleteGroup {
    private long group_id;
    private long user_id;
    private String adminId;

    public DeleteGroup(long group_id, long user_id, String adminId) {
        this.user_id = user_id;
        this.group_id = group_id;
        this.adminId = adminId;


    }
    public DeleteGroup(long group_id) {
        this.group_id = group_id;
    }

    public String getAdminId() {
        return adminId;
    }

    public long getUserId() {
        return user_id;
    }

    public long getGroup_id() {
        return group_id;
    }

}
