package com.chatlive.responsedata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

/**
 * Created by Intex pc on 2/16/2017.
 */

public class DeleteGroupResponse {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private DeleteResponse data;
    @SerializedName("errorData")
    @Expose
    private String errorData;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DeleteResponse getData() {
        return data;
    }

    public void setData(DeleteResponse data) {
        this.data = data;
    }

    public String getErrorData() {
        return errorData;
    }

    public void setErrorData(String errorData) {
        this.errorData = errorData;
    }

}
