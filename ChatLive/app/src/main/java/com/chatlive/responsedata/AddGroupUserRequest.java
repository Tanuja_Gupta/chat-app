package com.chatlive.responsedata;

import android.util.Log;

import org.json.JSONArray;

/**
 * Created by Intex pc on 2/10/2017.
 */

public class AddGroupUserRequest {
    private int group_id;
    private int user_id;
    private JSONArray user_array;

    public AddGroupUserRequest(int group_id, JSONArray user_array) {
        this.user_array = user_array;
        this.group_id = group_id;


    }

    public int getUserId() {
        return user_id;
    }

    public int getGroup_id() {
        return group_id;
    }

    public JSONArray getUser_array() {
        return user_array;
    }
}
