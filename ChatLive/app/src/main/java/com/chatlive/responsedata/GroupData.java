package com.chatlive.responsedata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Intex pc on 2/13/2017.
 */

public class GroupData {
    @SerializedName("group_users")
    @Expose
    private List<GroupUser> groupUsers = null;
    @SerializedName("group_detail")
    @Expose
    private GroupDetail groupDetail;

    public List<GroupUser> getGroupUsers() {
        return groupUsers;
    }

    public void setGroupUsers(List<GroupUser> groupUsers) {
        this.groupUsers = groupUsers;
    }

    public GroupDetail getGroupDetail() {
        return groupDetail;
    }

    public void setGroupDetail(GroupDetail groupDetail) {
        this.groupDetail = groupDetail;
    }
}