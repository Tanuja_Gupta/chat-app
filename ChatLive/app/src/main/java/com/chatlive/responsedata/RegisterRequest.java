package com.chatlive.responsedata;

/**
 * Created by Intex pc on 2/3/2017.
 */

public class RegisterRequest {
    private String phoneNumber;
    private String countryCode;
    private String ISO;

    public RegisterRequest(String countryCode, String phoneNumber, String ISO) {
        this.phoneNumber = phoneNumber;
        this.countryCode = countryCode;
        this.ISO = ISO;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getphoneNumber() {

        return phoneNumber;
    }

    public String getISO() {
        return ISO;
    }
}
