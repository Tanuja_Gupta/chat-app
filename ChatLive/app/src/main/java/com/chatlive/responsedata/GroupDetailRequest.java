package com.chatlive.responsedata;

/**
 * Created by Intex pc on 2/16/2017.
 */

public class GroupDetailRequest {
    private long group_id;

    public GroupDetailRequest(long group_id) {
        this.group_id = group_id;
    }

    public long getGroup_id() {
        return group_id;
    }

    public void setGroup_id(int group_id) {
        this.group_id = group_id;
    }
}
