package com.chatlive.ui;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.Contacts;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.chatlive.Adapters.UserListAdapter;
import com.chatlive.ChatLiveApplication;
import com.chatlive.R;
import com.chatlive.commonutils.CommonUtils;
import com.chatlive.commonutils.Constants;
import com.chatlive.commonutils.SessionManager;
import com.chatlive.data.MyContact;
import com.chatlive.responsedata.ChatUserData;
import com.chatlive.responsedata.UserData;
import com.chatlive.responsedata.UserListResponse;
import com.chatlive.syncclasses.ContactsObserver;
import com.chatlive.syncclasses.PerformSync;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, View.OnClickListener {
    private RecyclerView mRecyclerViewUser;
    private LinearLayoutManager mLayoutManager;
    private List<ChatUserData> userList;
    private UserListAdapter mAdapter;
    public static long userId;
    private TextView txtUser;
    private UserData userData;
    private ProgressDialog mProgDialog;
    ImageView btn_sync;
    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<String> mIDs = new ArrayList<>();
    private ArrayList<String> mNumbers = new ArrayList<>();
    private ArrayList<MyContact> contacts = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
        userData = SessionManager.getSavedObjectFromPreference(MainActivity.this,
                Constants.SHAREDPREFRENCE,
                "userData", UserData.class);
        userId = userData.getUserId();
        // WS call for getting all users and groups
        getUserList();

//        getContactDataAfter();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void init() {
//        initialising  Widgets
        btn_sync = (ImageView) findViewById(R.id.btn_sync);
        txtUser = (TextView) findViewById(R.id.txtUser);
        mRecyclerViewUser = (RecyclerView) findViewById(R.id.rcylViewUser);
        userList = new ArrayList<ChatUserData>();
        mRecyclerViewUser.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerViewUser.setLayoutManager(mLayoutManager);
        mAdapter = new UserListAdapter(userList, mRecyclerViewUser, getApplicationContext(), this);
        mRecyclerViewUser.setAdapter(mAdapter);
        findViewById(R.id.btn_addGroup).setOnClickListener(this);
        btn_sync.setOnClickListener(this);
    }

    // Get all users who are using chat app for now
    public void getUserList() {
        Call<UserListResponse> wsAdDetail;
        wsAdDetail = ((ChatLiveApplication) getApplicationContext())
                .mApiCall.getUserList(String.valueOf(userData.getUserId()));
        mProgDialog = new ProgressDialog(MainActivity.this);
        mProgDialog.setMessage(getString(R.string.please_wait));
        mProgDialog.setCancelable(false);
        mProgDialog.isIndeterminate();
        mProgDialog.show();

        wsAdDetail.enqueue(new Callback<UserListResponse>() {
            @Override
            public void onResponse(Call<UserListResponse> call, Response<UserListResponse> response) {
                if (mProgDialog.isShowing()) mProgDialog.dismiss();
                if (response.body() != null) {
                    userList.clear();
                    userList = response.body().getData();
                    setUserData(userList);
                } else if (response.errorBody() != null) {
                    CommonUtils.showValidationDialog(getApplicationContext(), "Oops! Some error occured");
                }
            }

            @Override
            public void onFailure(Call<UserListResponse> call, Throwable t) {
                t.printStackTrace();
                if (mProgDialog.isShowing()) mProgDialog.dismiss();
//               CommonUtils.showValidationDialog(getApplicationContext(), getString(R.string.server_error));
            }
        });
    }


    public void setUserData(List<ChatUserData> _userList) {
        this.userList = _userList;
        mAdapter.clear();
        mAdapter.addNewList(userList);
        txtUser.setText(userList.size() + " Users");

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        Intent intent = new Intent(MainActivity.this, ChatActivity.class);
        if (userList.get(position).getGroupId().length() <= 0) {
            intent.putExtra(Constants.DESTINATIONID, userList.get(position).getUserId());
        } else {
            intent.putExtra(Constants.GROUPADMIN, userList.get(position).getAdminId());
            intent.putExtra(Constants.DESTINATIONID, userList.get(position).getGroupId());
        }
        intent.putExtra(Constants.PHONENUM, userList.get(position).getPhoneNumber());
        intent.putExtra(Constants.DESTINATIONTYPE, userList.get(position).getType());
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_addGroup) {
            Intent i = new Intent(MainActivity.this, AddGroupActivity.class);
            Bundle messageBundle = new Bundle();
            messageBundle.putSerializable("list", (Serializable) userList);
            i.putExtras(messageBundle);
            startActivity(i);
        }
        if (v.getId() == R.id.btn_sync) {
// Force Sync for refreshing Contacts /**/
            PerformSync prSyn = new PerformSync(MainActivity.this);
            prSyn.startForceSync();

        }

    }
    private String getCurrentAccount() {
        Account[] accounts = AccountManager.get(this).getAccountsByType(getString(R.string.ACCOUNT_TYPE));
        return accounts.length < 0 ? "" : accounts[0].name;
    }

    private JSONArray getContactDataAfter() {
        Cursor c = getContentResolver()
                .query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);

        List<String> RIds = new ArrayList<>();
        mIDs = new ArrayList<>();
        mNumbers = new ArrayList<>();
        int i = 0;
        JSONArray jsonArray = new JSONArray();
        if (c != null && c.moveToFirst()) {
            do {
                mIDs.add(c.getString(c
                        .getColumnIndexOrThrow(ContactsContract.Contacts._ID)));
                mNames.add(c.getString(c
                        .getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME)));

                Cursor c2 = getContentResolver()
                        .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER},
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?",
                                new String[]{mIDs.get(i)}, null);

                if (c2 != null && c2.moveToFirst()) {
                    do {
                        mNumbers.add(c2.getString(c2
                                .getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER)));
                    } while (c2.moveToNext());
                    c2.close();
                }

                Cursor rawcontacts = getContentResolver()
                        .query(ContactsContract.RawContacts.CONTENT_URI,
                                new String[]{ContactsContract.RawContacts._ID},
                                ContactsContract.RawContacts.CONTACT_ID + "=?",
                                new String[]{mIDs.get(i)}, null);

                if (rawcontacts != null && rawcontacts.moveToFirst()) {
                    do {
                        RIds.add(rawcontacts.getString(rawcontacts
                                .getColumnIndexOrThrow(ContactsContract.RawContacts._ID)));
                    } while (rawcontacts.moveToNext());
                    rawcontacts.close();
                }
//                "Bearer " + userData.getAuthToken(
                Log.e("Contact after Sync  ", jsonArray.toString());
                Log.e("Contact after Sync  ", mNumbers.get(i));
                i++;
            } while (c.moveToNext());
            c.close();
        }
        return jsonArray;
    }
}
