package com.chatlive.ui;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.chatlive.Adapters.ChatAdapter;
import com.chatlive.ChatLiveApplication;
import com.chatlive.R;
import com.chatlive.callbacks.SocketIOManager;
import com.chatlive.commonutils.BundleKeys;
import com.chatlive.commonutils.CommonUtils;
import com.chatlive.commonutils.Constants;
import com.chatlive.commonutils.JsonUtil;
import com.chatlive.commonutils.SessionManager;
import com.chatlive.data.ChatMessage;
import com.chatlive.database.DatabaseHandler;
import com.chatlive.responsedata.ChatFileResponse;
import com.chatlive.responsedata.UserData;
import com.chatlive.services.ChatService;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Intex pc on 1/31/2017.
 */
public class ChatActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {
    EditText chatMessageEditText;
    private SocketIOManager socketIOManager;
    private long destination_id;
    private long fromuserID;
    private Button chatMessageSendBtn;
    private Button btn_muteChat;
    private LinearLayout lyt_cha_image;
    private List<ChatMessage> chatMessageList = new ArrayList<>();
    RecyclerView lstChat;
    int destinationtype;
    private UserData userData;
    private JsonUtil jsonUtil;
    private String userChoosenTask;
    private int REQUEST_CAMERA = 0;
    private int SELECT_FILE = 1;
    private int PICK_VIDEO_REQUEST = 2;
    private int REQUEST_VIDEO_CAMERA = 3;
    private ProgressDialog mProgDialog;
    DatabaseHandler db;
    private int chatId;
    ChatMessage chatmesage;
    private String phone;
    private String admin;
    private String thum = null;
    private Button txtGroupDetails;
    private int mute = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        //Register BroadcastReceiver
        //to receive event from our service
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ChatService.UPDATE_USER);
        Intent serviceIntent = new Intent(this, ChatService.class);
        startService(serviceIntent);
        socketIOManager = SocketIOManager.getInstance();
        jsonUtil = JsonUtil.getInstance();
        userData = SessionManager.getSavedObjectFromPreference(ChatActivity.this,
                Constants.SHAREDPREFRENCE,
                Constants.USERDATA, UserData.class);
        fromuserID = userData.getUserId();
        // Getting bundle from Intent to check whether chatting type is single user or group
        // (variable is destinationtype 0 for single chat and 1 for group)
        Bundle bndl = getIntent().getExtras();
        if (bndl != null) {
            destination_id = Long.parseLong(bndl.getString(Constants.DESTINATIONID));
            destinationtype = bndl.getInt(Constants.DESTINATIONTYPE);
            if (bndl.containsKey(Constants.PHONENUM)) {
                phone = bndl.getString(Constants.PHONENUM);
            }
            if (bndl.containsKey(Constants.GROUPADMIN)) {
                admin = bndl.getString(Constants.GROUPADMIN);
            }

        }
        // Registering broadcast for receiving messages passed from ChatService
        IntentFilter chatBroadcastIntentFilter = new IntentFilter();
        chatBroadcastIntentFilter.addAction(Constants.PING_CONSTANTS.PING_ACTION);
        registerReceiver(chatReceiver, chatBroadcastIntentFilter);
        findIds();
        // Checking for chatting in local db to retreive messages history
        // if chat exists get all message
        //else create new chatting id in table to store received messages
        if (db.chatExist(destination_id) != 0) {
            chatId = db.chatExist(destination_id);
            mute = db.getMuteStatus(chatId);
            chatMessageList = db.getChatMessage(chatId);
            if (chatMessageList.size() > 0) {
                adapter.setChatList(chatMessageList);
                adapter.notifyDataSetChanged();
            }
        } else {
            db.addRecentChatUser(destination_id, phone, destinationtype, mute);
            chatId = db.chatExist(destination_id);
            mute = db.getMuteStatus(chatId);
        }

        if (mute == 0) {
            btn_muteChat.setText("Mute");
        } else {
            btn_muteChat.setText("Unmute");

        }

        // update unread chat to read mark
        updateUnreadChat(chatId);

    }

    public void updateUnreadChat(int chatID) {
        List<ChatMessage> unreadChat = db.getUnreadChat(chatId, 0);
        if (unreadChat.size() > 0) {
            for (int i = 0; i < unreadChat.size(); i++) {
                db.updateChatRead(chatID, 0);
            }
        }
    }

    public void findIds() {
        db = new DatabaseHandler(this);
        lyt_cha_image = (LinearLayout) findViewById(R.id.lyt_cha_image);
        lyt_cha_image.setVisibility(View.VISIBLE);
        findViewById(R.id.btn_video).setOnClickListener(this);
        findViewById(R.id.btn_image).setOnClickListener(this);
        txtGroupDetails = (Button) findViewById(R.id.txtGroupDetails);
        txtGroupDetails.setOnClickListener(this);
        if (destinationtype == 1) {
            txtGroupDetails.setVisibility(View.VISIBLE);
        } else {
            txtGroupDetails.setVisibility(View.GONE);
        }
        lstChat = (RecyclerView) findViewById(R.id.chatMessageList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, OrientationHelper.VERTICAL, false);
        lstChat.setLayoutManager(linearLayoutManager);
        lstChat.setItemAnimator(new DefaultItemAnimator());
        adapter = new ChatAdapter(this, this);
        adapter.setChatList(chatMessageList);
        lstChat.setAdapter(adapter);
        chatMessageSendBtn = (Button) findViewById(R.id.chatMessageSendBtn);
        btn_muteChat = (Button) findViewById(R.id.btn_muteChat);
        btn_muteChat.setOnClickListener(this);
        chatMessageSendBtn.setOnClickListener(this);
        chatMessageEditText = (EditText) findViewById(R.id.chatMessageEditText);
        chatMessageEditText.addTextChangedListener(messageWatcher);

    }

    private Timer typingTimer = new Timer();
    private final TextWatcher messageWatcher = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(final Editable s) {
            //   socketIOManager.emit(SocketIOManager.MESSAGE_TYPE.TYPE_START, jsonUtil.toJson(setMessage(s.toString(), 0, null)));
            sendTypeStart();
            typingTimer.cancel();
            typingTimer = new Timer();
            typingTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    sendTypeEnd();
                    //      socketIOManager.emit(SocketIOManager.MESSAGE_TYPE.TYPE_END, jsonUtil.toJson(setMessage(s.toString(), 0, null)));

                }
            }, 1000);
        }
    };

    private ChatMessage setMessage(String message, int messageType, String thumnail) {
        // handling typing events
        ChatMessage typingMessage = new ChatMessage();
        typingMessage.setToUserId(destination_id);
        typingMessage.setMessage(message);
        typingMessage.setFromUserId(fromuserID);
        typingMessage.setCreatedOn(System.currentTimeMillis());
        typingMessage.setMessageType(messageType);
        typingMessage.setThumb_path(thumnail);
        typingMessage.setDestination_type(destinationtype);
        typingMessage.setMute(mute);
        this.chatmesage = typingMessage;
        return typingMessage;
    }

    private ChatAdapter adapter;
    private BroadcastReceiver chatReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ChatMessage message = (ChatMessage) intent.getExtras().getSerializable(BundleKeys.PING_BUNDLE);
            String type = intent.getStringExtra(BundleKeys.PING_TYPE_BUNDLE);
            if (type.equals(SocketIOManager.MESSAGE_TYPE.PING) && message.getMessage() != null) {
                if (destination_id == message.getFromUserId() && destinationtype == message.getDestination_type()) {
                    chatMessageList.add(chatMessageList.size(), message);
                    adapter.setChatList(chatMessageList);
                    adapter.notifyDataSetChanged();
                    if (destinationtype == 0) {
                        if (db.chatExist(message.getFromUserId()) != 0) {
                            chatId = db.chatExist(message.getFromUserId());
                            db.addMessage(chatId, message);
                        } else {
                            // need to get phone number for sending in second argument
                            db.addRecentChatUser(message.getFromUserId(), "", message.getDestination_type(), mute);
                            chatId = db.chatExist(message.getFromUserId());
                            db.addMessage(chatId, message);
                        }
                    }
//                    if (destinationtype == 1) {
//                        if (db.chatExist(message.getFromUserId()) != 0) {
//                            chatId = db.chatExist(message.getFromUserId());
//                            db.addMessage(chatId, message);
//                        } else {
//                            // need to get phone number for sending in second argument
//                            db.addRecentChatUser(message.getFromUserId(), "", message.getDestination_type(), mute);
//                            chatId = db.chatExist(message.getFromUserId());
//                            db.addMessage(chatId, message);
//                        }
//                    }
                }
                if (destination_id == message.getToUserId() && destinationtype == message.getDestination_type()) {
                    chatMessageList.add(chatMessageList.size(), message);
                    adapter.setChatList(chatMessageList);
                    adapter.notifyDataSetChanged();
                    if (destinationtype == 1) {
                        if (db.chatExist(message.getFromUserId()) != 0) {
                            chatId = db.chatExist(message.getToUserId());
                            db.addMessage(chatId, message);
                        } else {
                            // need to get phone number for sending in second argument
                            db.addRecentChatUser(message.getToUserId(), "", message.getDestination_type(), mute);
                            chatId = db.chatExist(message.getToUserId());
                            db.addMessage(chatId, message);
                        }
                    }
                } else {
                    if (message.getDestination_type() == 0) {
                        if (db.chatExist(message.getFromUserId()) != 0) {
                            chatId = db.chatExist(message.getFromUserId());
                            db.addMessage(chatId, message);
                        } else {
                            // need to get phone number for sending in second argument
                            db.addRecentChatUser(message.getFromUserId(), "", message.getDestination_type(), mute);
                            chatId = db.chatExist(message.getFromUserId());
                            db.addMessage(chatId, message);
                        }
                    }
                    if (message.getDestination_type() == 1) {
                        if (db.chatExist(message.getToUserId()) != 0) {
                            chatId = db.chatExist(message.getToUserId());
                            db.addMessage(chatId, message);
                        } else {
                            // need to get phone number for sending in second argument
                            db.addRecentChatUser(message.getToUserId(), "", message.getDestination_type(), mute);
                            chatId = db.chatExist(message.getToUserId());
                            db.addMessage(chatId, message);
                        }
                    }
                }
            }
            if (type.equals(SocketIOManager.MESSAGE_TYPE.LEFTUSER) && message.getMessage() != null) {
                if (destination_id == message.getToUserId()) {
                    chatMessageList.add(chatMessageList.size(), message);
                    adapter.setChatList(chatMessageList);
                    adapter.notifyDataSetChanged();
                }
                if (db.chatExist(message.getToUserId()) != 0) {
                    int chattingId = db.chatExist(message.getToUserId());
                    db.addMessage(chattingId, message);
                } else {
                    db.addRecentChatUser(message.getToUserId(), "", message.getDestination_type(), mute);
                    int chattingId = db.chatExist(message.getToUserId());
                    db.addMessage(chattingId, message);
                }
            }

        }
    };


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.chatMessageSendBtn:
                sendMeesage(chatMessageEditText.getText().toString(), 0, null);
                break;
            case R.id.btn_image:
                selectImage();
                break;

            case R.id.btn_video:
                selectVideo();
                break;
            case R.id.txtGroupDetails:
                // sending group details to Groupdetail activity
                Intent i = new Intent(ChatActivity.this, GroupDetailActivity.class);
                i.putExtra(Constants.GROUPID, destination_id);
                i.putExtra(Constants.GROUPADMIN, admin);
                startActivity(i);
                break;
            case R.id.btn_muteChat:
                if (mute == 0) {
                    db.updateMuteStatus(chatId, 1);
                    mute = db.getMuteStatus(chatId);
                    btn_muteChat.setText("Unmute");
                } else {
                    db.updateMuteStatus(chatId, 0);
                    mute = db.getMuteStatus(chatId);
                    btn_muteChat.setText("Mute");
                }
                break;
        }
    }

    // Sending message to socket
    public void sendMeesage(String message, int messageType, String thum) {
        if (messageType == 0) {
            // adding message to local db
            db.addMessage(chatId, setMessage(message, messageType, null));
        }
        chatMessageList.add(chatMessageList.size(), setMessage(message, messageType, null));
        adapter.notifyDataSetChanged();
        socketIOManager.emit(SocketIOManager.MESSAGE_TYPE.PING, jsonUtil.toJson(setMessage(message, messageType, thum)));
        chatMessageEditText.setText("");

    }

    // Sending Typing start to socket
    public void sendTypeStart() {
        ChatMessage typingMessage = new ChatMessage();
        typingMessage.setToUserId(destination_id);
        typingMessage.setFromUserId(fromuserID);
        typingMessage.setDestination_type(destinationtype);
        socketIOManager.emit(SocketIOManager.MESSAGE_TYPE.TYPE_START, jsonUtil.toJson(typingMessage));

    }

    // Sending Typing message to socket
    public void sendTypeEnd() {
        ChatMessage typingMessage = new ChatMessage();
        typingMessage.setToUserId(destination_id);
        typingMessage.setFromUserId(fromuserID);
        typingMessage.setDestination_type(destinationtype);
        socketIOManager.emit(SocketIOManager.MESSAGE_TYPE.TYPE_START, jsonUtil.toJson(typingMessage));

    }

    // Dialog to select photo from camera and gallery
    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Gallery",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(ChatActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = CommonUtils.checkPermission(ChatActivity.this);
                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        cameraIntent();
                } else if (items[item].equals("Choose from Gallery")) {
                    userChoosenTask = "Choose from Gallery";
                    if (result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    // Dialog to select Video from camera and gallery
    private void selectVideo() {
        final CharSequence[] items = {"Take Video", "Video from Gallery",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(ChatActivity.this);
        builder.setTitle("Add Video!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = CommonUtils.checkPermission(ChatActivity.this);
                if (items[item].equals("Take Video")) {
                    userChoosenTask = "Take Video";
                    if (result)
                        captureVideoRequest();
                } else if (items[item].equals("Video from Gallery")) {
                    userChoosenTask = "Video from Gallery";
                    if (result)
                        galleryVideo();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    // Checking permissions for Storage
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case CommonUtils.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Gallery"))
                        galleryIntent();
                } else {
                    // finish();
                }
                break;
            case CommonUtils.MY_PERMISSIONS_REQUEST_CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Video"))
                        captureVideoRequest();
                    else if (userChoosenTask.equals("Video from Gallery"))
                        galleryVideo();
                } else {
                }
                break;
        }
    }

    // Gallery intent for choosing video
    public void galleryVideo() {
        Intent intent = new Intent();
        intent.setType("video/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Video"), PICK_VIDEO_REQUEST);
    }

    // Gallery intent for choosing image
    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select Image"), SELECT_FILE);
    }

    // Camera intent for choosing image
    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    // Getting image data from Bundle camera
    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        // convert byte array to base64
        File destination = new File(Constants.FILEPATH,
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
            if (destination.exists()) {
                db.addMessage(chatId, setMessage(destination.getAbsolutePath(), 1, null));
                uploadFile(1, destination.getAbsolutePath());
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    // getting image data from Gallery
    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                // change bitmap to byte array
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();
                // convert byte array to base64
                String base64 = Base64.encodeToString(byteArray, Base64.DEFAULT);
                Uri tempUri = CommonUtils.getImageUri(ChatActivity.this, bm);
                String filepath = CommonUtils.getRealPathFromURI(getApplicationContext(), tempUri);
                // convert base64 to  byte array
                // bm = CommonUtils.base64ToBitmap(base64);
                File destination = new File(filepath);
                if (destination.exists()) {
                    db.addMessage(chatId, setMessage(filepath, 1, null));
                    uploadFile(1, filepath);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                onSelectFromGalleryResult(data);
            } else if (requestCode == REQUEST_CAMERA) {
                onCaptureImageResult(data);
            } else if (requestCode == PICK_VIDEO_REQUEST) {
                String videoPath = generatePath(data.getData(), ChatActivity.this);
                uploadFile(2, videoPath);
                db.addMessage(chatId, setMessage(videoPath, 2, null));
            } else if (requestCode == REQUEST_VIDEO_CAMERA) {
                String videoPath = getVideoPath(data);
                if (videoPath != null) {
                    uploadFile(2, videoPath);
                    db.addMessage(chatId, setMessage(videoPath, 2, null));
                }
            }


        }
    }

    private String getVideoPath(Intent data) {
        try {
            AssetFileDescriptor videoAsset = getContentResolver().openAssetFileDescriptor(data.getData(), "r");
            FileInputStream fis = videoAsset.createInputStream();
            File root = new File(Environment.getExternalStorageDirectory(), "/Chat Live/");  //you can replace RecordVideo by the specific folder where you want to save the video
            if (!root.exists()) {
                System.out.println("No directory");
                root.mkdirs();
            }
            File file;
            file = new File(root, "Video_" + System.currentTimeMillis() + ".mp4");
            FileOutputStream fos = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len;
            while ((len = fis.read(buf)) > 0) {
                fos.write(buf, 0, len);
            }
            fis.close();
            fos.close();
            String videoPath = file.getAbsolutePath();
            return videoPath;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }


    public String generatePath(Uri uri, Context context) {
        String filePath = null;
        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        if (isKitKat) {
            filePath = generateFromKitkat(uri, context);
        }
        if (filePath != null) {
            return filePath;
        }
        Cursor cursor = context.getContentResolver().query(uri, new String[]{MediaStore.MediaColumns.DATA}, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
        }
        return filePath == null ? uri.getPath() : filePath;
    }

    @TargetApi(19)
    private String generateFromKitkat(Uri uri, Context context) {
        String filePath = null;
        if (DocumentsContract.isDocumentUri(context, uri)) {
            String wholeID = DocumentsContract.getDocumentId(uri);
            String id = wholeID.split(":")[1];
            String[] column = {MediaStore.Video.Media.DATA};
            String sel = MediaStore.Video.Media._ID + "=?";
            Cursor cursor = context.getContentResolver().
                    query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                            column, sel, new String[]{id}, null);
            int columnIndex = cursor.getColumnIndex(column[0]);

            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
        }
        return filePath;
    }

    public void captureVideoRequest() {
        //  start the Video Capture Intent
        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAMERA);
    }

    public void uploadFile(final int messageType, final String path) {
        MultipartBody.Part imagePath = null;
        MultipartBody.Part videoPath = null;
        if (path != null) {
            if (messageType == 1) {
                File file = new File(path);
                RequestBody requestFile =
                        RequestBody.create(MediaType.parse("multipart/form-data"), file);
                imagePath = MultipartBody.Part.createFormData("chatImage", file.getName(), requestFile);
            } else {
                File file = new File(path);
                RequestBody requestFile =
                        RequestBody.create(MediaType.parse("multipart/form-data"), file);
                videoPath = MultipartBody.Part.createFormData("chatVideo", file.getName(), requestFile);
            }
        }
        // adding another part within the multipart request for rest of fields
        RequestBody type = CommonUtils.getRequestBody(String.valueOf(messageType));
        RequestBody userID = CommonUtils.getRequestBody(String.valueOf(userData.getUserId()));
        if (CommonUtils.checkInternetConnection(this)) {

            mProgDialog = ProgressDialog.show(ChatActivity.this, "",
                    getString(R.string.please_wait), true);
            mProgDialog.setCancelable(false);
            mProgDialog.isIndeterminate();
            Call<ChatFileResponse> wsOTP = ((ChatLiveApplication) getApplicationContext())
                    .mApiCall.uploadFile(userID, type, imagePath, videoPath);
            wsOTP.enqueue(new Callback<ChatFileResponse>() {
                @Override
                public void onResponse(Call<ChatFileResponse> call, Response<ChatFileResponse> response) {
                    mProgDialog.dismiss();
                    if (response.body() != null) {
                        if (response.body().getStatus().equalsIgnoreCase(Constants.SUCCESS)) {
                            Toast.makeText(getApplicationContext(), "uploaded", Toast.LENGTH_LONG).show();
                            if (messageType == 2) {
                                thum = response.body().getThumb_path();
                            }
                            sendMeesage(response.body().getUpload_file(), response.body().getMessageType(), thum);
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "error in uploading", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ChatFileResponse> call, Throwable t) {
                    mProgDialog.dismiss();
                    CommonUtils.showValidationDialog(ChatActivity.this, getString(R.string.server_error));
                }
            });
        }
    }

    // unregsitering chat Event
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (chatReceiver != null) {
            unregisterReceiver(chatReceiver);
        }
        Intent serviceIntent = new Intent(this, ChatService.class);
        if (isServiceRunning(ChatService.class)) {
            stopService(serviceIntent);
        }
    }

    // Checking service is running or not
    public boolean isServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) this
                .getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager
                .getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (chatMessageList.get(position).getMessageType() == 1) {
            String name = chatMessageList.get(position).getMessage().substring(chatMessageList.get(position).getMessage().lastIndexOf("/") + 1);
            if (!new File(Constants.FILEPATH + name).exists()) {
                new DownloadFileFromURL().execute(chatMessageList.get(position).getMessage(), name);
            } else {
                Toast.makeText(getApplicationContext(), "Image Already stored ", Toast.LENGTH_SHORT).show();
            }
        }
        if (chatMessageList.get(position).getMessageType() == 2) {


            String name = chatMessageList.get(position).getMessage().substring(chatMessageList.get(position).getMessage().lastIndexOf("/") + 1);
            if (!new File(Constants.FILEPATH + name).exists()) {
                new DownloadFileFromURL().execute(chatMessageList.get(position).getMessage(), name);
                if (chatMessageList.get(position).getThumb_path() != null) {
                    new DownloadFileFromURL().execute(chatMessageList.get(position).getThumb_path(), "thumbnail_" + chatMessageList.get(position).getThumb_path().substring(chatMessageList.get(position).getThumb_path().lastIndexOf("/") + 1));
                }
            } else {
                Toast.makeText(getApplicationContext(), "Already stored ", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * Background Async Task to download file
     */
    class DownloadFileFromURL extends AsyncTask<String, String, String> {
        ProgressDialog PD;

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            PD = ProgressDialog.show(ChatActivity.this, null, "Please Wait ...", true);
            PD.setCancelable(true);
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            String fileName;
            fileName = f_url[1];
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                String rootDir = Environment.getExternalStorageDirectory()
                        + File.separator + "Chat Live";
                File rootFile = new File(rootDir);
                if (!rootFile.exists()) {
                    rootFile.mkdir();
                }
                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);
                fileName = rootDir + File.separator + fileName;
                // Output stream to write file
                OutputStream output = new FileOutputStream(fileName);

                byte data[] = new byte[1024];
                while ((count = input.read(data)) != -1) {
                    // writing data to file
                    output.write(data, 0, count);
                }
                // flushing output
                output.flush();
                // closing streams
                output.close();
                input.close();
                return fileName;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        /**
         * After completing background task
         * Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            PD.dismiss();
        }

    }


}


