package com.chatlive.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.chatlive.Adapters.AddMemberAdapter;
import com.chatlive.ChatLiveApplication;
import com.chatlive.R;
import com.chatlive.callbacks.SocketIOManager;
import com.chatlive.commonutils.BundleKeys;
import com.chatlive.commonutils.CommonUtils;
import com.chatlive.commonutils.Constants;
import com.chatlive.commonutils.JsonUtil;
import com.chatlive.commonutils.SessionManager;
import com.chatlive.data.ChatMessage;
import com.chatlive.responsedata.AddGroupRequest;
import com.chatlive.responsedata.AddGroupResponse;
import com.chatlive.responsedata.AddGroupUserRequest;
import com.chatlive.responsedata.ChatUserData;
import com.chatlive.responsedata.SingleUser;
import com.chatlive.responsedata.UserData;
import com.chatlive.services.ChatService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddGroupActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    EditText et_groupName;
    UserData userData;
    Button btn_AddGroup;
    List<ChatUserData> mList;
    private ProgressDialog mProgDialog;
    private RecyclerView recyclerView;
    private AddMemberAdapter adapter;
    GestureDetectorCompat gestureDetector;
    private int group_id;
    private Button btn_AddMember;
    private SocketIOManager socketIOManager;
    private JsonUtil jsonUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_group);
        Bundle bndl = getIntent().getExtras();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ChatService.UPDATE_USER);
        Intent serviceIntent = new Intent(this, ChatService.class);
        startService(serviceIntent);
        socketIOManager = SocketIOManager.getInstance();

        jsonUtil = JsonUtil.getInstance();
        initView();
        if (bndl != null) {
            mList = (List<ChatUserData>) bndl.getSerializable("list");
        }
        setListView();
        userData = SessionManager.getSavedObjectFromPreference(AddGroupActivity.this,
                Constants.SHAREDPREFRENCE,
                Constants.USERDATA, UserData.class);
    }

    public void initView() {
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        et_groupName = (EditText) findViewById(R.id.et_groupName);
        btn_AddGroup = (Button) findViewById(R.id.btn_AddGroup);
        btn_AddMember = (Button) findViewById(R.id.btn_AddMember);
        btn_AddMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (group_id != 0) {

                    if (adapter.getSelectedItems().size() <= 0) {
                        Toast.makeText(getApplicationContext(), "Please Select Members", Toast.LENGTH_SHORT).show();
                    } else {

                        JSONArray jsonArray = null;
                        if (adapter.getSelectedItems().size() > 0) {
                            jsonArray = new JSONArray();
                            for (int i = 0; i < adapter.getSelectedItems().size(); i++) {
                                JSONObject job = new JSONObject();
                                try {
                                    job.put("userId", mList.get(adapter.getSelectedItems().get(i)).getUserId());
                                    job.put("phoneNumber", mList.get(adapter.getSelectedItems().get(i)).getPhoneNumber());
                                    jsonArray.put(job);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            addGroupUserWS(jsonArray, group_id);


                        }

                    }
                }
            }
        });
        btn_AddGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_groupName.getText().toString().length() != 0) {
                    addGroupWS();
                } else {
                    Log.e("Enter", "Enter Text");
                }

            }
        });
    }

    public void addGroupWS() {
        // adding another part within the multipart request for rest of fields
        if (CommonUtils.checkInternetConnection(this)) {
            AddGroupRequest request = new AddGroupRequest(et_groupName.getText().toString(), userData.getUserId());
            mProgDialog = ProgressDialog.show(AddGroupActivity.this, "",
                    getString(R.string.please_wait), true);
            mProgDialog.setCancelable(false);
            mProgDialog.isIndeterminate();
            Call<AddGroupResponse> wsOTP = ((ChatLiveApplication) getApplicationContext())
                    .mApiCall.addGroup(request, "Bearer " + userData.getAuthToken());
            wsOTP.enqueue(new Callback<AddGroupResponse>() {
                @Override
                public void onResponse(Call<AddGroupResponse> call, Response<AddGroupResponse> response) {
                    mProgDialog.dismiss();
                    if (response.body() != null) {
                        if (response.body().getStatus().equalsIgnoreCase(Constants.SUCCESS)) {
                            group_id = response.body().getData().getGroupDetail().getGroupId();
                            Toast.makeText(getApplicationContext(), "success\n", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "errror\n", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<AddGroupResponse> call, Throwable t) {
                    mProgDialog.dismiss();
                    CommonUtils.showValidationDialog(AddGroupActivity.this, getString(R.string.server_error));
                }
            });
        }
    }

    public void setListView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        // actually VERTICAL is the default,
        // just remember: LinearLayoutManager
        // supports HORIZONTAL layout out of the box
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        // you can set the first visible item like this:
        layoutManager.scrollToPosition(0);
        recyclerView.setLayoutManager(layoutManager);
        // allows for optimizations if all items are of the same size:
        recyclerView.setHasFixedSize(true);
        adapter = new AddMemberAdapter(mList, recyclerView, getApplicationContext(), this);
        recyclerView.setAdapter(adapter);
        RecyclerView.ItemDecoration itemDecoration =
                new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(itemDecoration);
        // this is the default; this call is actually only necessary with custom ItemAnimators
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }


    private void myToggleSelection(int idx) {
        adapter.toggleSelection(idx);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // Select and unselect users
        myToggleSelection(position);
    }

    public void addGroupUserWS(final JSONArray jsonArray, final int group_id) {
        // adding another part within the multipart request for rest of fields
        if (CommonUtils.checkInternetConnection(this)) {
            AddGroupUserRequest request = new AddGroupUserRequest(group_id, jsonArray);
            mProgDialog = ProgressDialog.show(AddGroupActivity.this, "",
                    getString(R.string.please_wait), true);
            mProgDialog.setCancelable(false);
            mProgDialog.isIndeterminate();
            Call<AddGroupResponse> wsOTP = ((ChatLiveApplication) getApplicationContext())
                    .mApiCall.addUserGroup(request, "Bearer " + userData.getAuthToken());
            wsOTP.enqueue(new Callback<AddGroupResponse>() {
                @Override
                public void onResponse(Call<AddGroupResponse> call, Response<AddGroupResponse> response) {
                    mProgDialog.dismiss();
                    if (response.body() != null) {
                        if (response.body().getStatus().equalsIgnoreCase(Constants.SUCCESS)) {
                            int group_size = response.body().getData().getGroupUsers().size();

                            Toast.makeText(getApplicationContext(), group_size + " success\n", Toast.LENGTH_LONG).show();

                            if (socketIOManager.isConnected) {
                                Log.e("DATA", "connected");
                                socketIOManager.emit(SocketIOManager.MESSAGE_TYPE.ADDUSER, setMessage(et_groupName.getText().toString(), group_id, jsonArray, "Created " + et_groupName.getText().toString() + " and Added you in group"));

                            } else {
                                IntentFilter intentFilter = new IntentFilter();
                                intentFilter.addAction(ChatService.UPDATE_USER);
                                Intent serviceIntent = new Intent(AddGroupActivity.this, ChatService.class);
                                startService(serviceIntent);
                                socketIOManager = SocketIOManager.getInstance();
                                socketIOManager.emit(SocketIOManager.MESSAGE_TYPE.ADDUSER, setMessage(et_groupName.getText().toString(), group_id, jsonArray, "Created " + et_groupName.getText().toString() + " and Added you in group"));
                            }
                        }


                    } else {
                        Toast.makeText(getApplicationContext(), "errror\n", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<AddGroupResponse> call, Throwable t) {
                    mProgDialog.dismiss();
                    CommonUtils.showValidationDialog(AddGroupActivity.this, getString(R.string.server_error));
                }
            });
        }
    }

    private JSONObject setMessage(String group_name, int group_id, JSONArray userArray, String message) {
        // handling typing events
        JSONObject jobj = new JSONObject();
        try {
            jobj.put("destination_id", group_id);
            jobj.put("group_name", group_name);
            jobj.put("message", message);
            jobj.put("userList", userArray);
            jobj.put("destination_type", 1);
            jobj.put("messageType", 3);
            jobj.put("created_on", System.currentTimeMillis());
            return jobj;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
