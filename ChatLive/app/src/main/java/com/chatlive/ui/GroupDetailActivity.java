package com.chatlive.ui;

import android.app.IntentService;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Toast;

import com.chatlive.Adapters.GroupListAdapter;
import com.chatlive.Adapters.UserListAdapter;
import com.chatlive.ChatLiveApplication;
import com.chatlive.R;
import com.chatlive.callbacks.SocketIOManager;
import com.chatlive.commonutils.BundleKeys;
import com.chatlive.commonutils.CommonUtils;
import com.chatlive.commonutils.Constants;
import com.chatlive.commonutils.SessionManager;
import com.chatlive.data.ChatMessage;
import com.chatlive.database.DatabaseHandler;
import com.chatlive.responsedata.ChatUserData;
import com.chatlive.responsedata.DeleteGroup;
import com.chatlive.responsedata.DeleteGroupResponse;
import com.chatlive.responsedata.GroupDetail;
import com.chatlive.responsedata.GroupDetailRequest;
import com.chatlive.responsedata.GroupDetailsResponse;
import com.chatlive.responsedata.GroupUser;
import com.chatlive.responsedata.UserData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GroupDetailActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private ProgressDialog mProgDialog;
    private UserData userData;
    private long destination_id;
    private int fromuserID;
    private RecyclerView mRecyclerViewUser;
    List<GroupUser> userList;
    LinearLayoutManager mLayoutManager;
    private GroupListAdapter mAdapter;
    private String admin;
    private Button btn_deleteGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_detail);
        init();
        userData = SessionManager.getSavedObjectFromPreference(GroupDetailActivity.this,
                Constants.SHAREDPREFRENCE,
                Constants.USERDATA, UserData.class);
        Bundle bndl = getIntent().getExtras();
        if (bndl != null) {
            destination_id = bndl.getLong(Constants.GROUPID);
            if (bndl.containsKey(Constants.GROUPADMIN)) {
                admin = bndl.getString(Constants.GROUPADMIN);
            }
        }
        // Registering broadcast for receiving messages passed from ChatService
        IntentFilter chatBroadcastIntentFilter = new IntentFilter();
        chatBroadcastIntentFilter.addAction(Constants.PING_CONSTANTS.PING_ACTION);
        registerReceiver(userReceiver, chatBroadcastIntentFilter);
        fromuserID = userData.getUserId();
        getGroupDetails();
    }

    // initialzation of the views
    private void init() {
        btn_deleteGroup = (Button) findViewById(R.id.btn_deleteGroup);
        mRecyclerViewUser = (RecyclerView) findViewById(R.id.rcylViewUser);
        userList = new ArrayList<GroupUser>();
        mRecyclerViewUser.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerViewUser.setLayoutManager(mLayoutManager);
        mAdapter = new GroupListAdapter(userList, mRecyclerViewUser, getApplicationContext(), this);
        mRecyclerViewUser.setAdapter(mAdapter);
        btn_deleteGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (String.valueOf(fromuserID).equals(admin)) {
                    deleteGroup(destination_id);
                } else {
                    Toast.makeText(getApplicationContext(), "You can not delete this group", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (userReceiver != null) {
            unregisterReceiver(userReceiver);
        }
    }

    // delete single user from the Group, If user i snot admin he can delete himself but not other members
    public void deleteGroupUser(final int userId, final int position) {
        if (CommonUtils.checkInternetConnection(this)) {
            DeleteGroup request = new DeleteGroup(destination_id, userId, admin);
            mProgDialog = ProgressDialog.show(GroupDetailActivity.this, "",
                    getString(R.string.please_wait), true);
            mProgDialog.setCancelable(false);
            mProgDialog.isIndeterminate();
            Call<DeleteGroupResponse> wsOTP = ((ChatLiveApplication) getApplicationContext())
                    .mApiCall.deleteGroupUser(request, "Bearer " + userData.getAuthToken());
            wsOTP.enqueue(new Callback<DeleteGroupResponse>() {
                              @Override
                              public void onResponse(Call<DeleteGroupResponse> call, Response<DeleteGroupResponse> response) {
                                  mProgDialog.dismiss();
                                  if (response.body() != null) {
                                      SocketIOManager socketIOManager = SocketIOManager.getInstance();
                                      if (response.body().getStatus().equalsIgnoreCase(Constants.SUCCESS)) {
                                          try {
                                              String message = response.body().getData().getMessage();
                                              String admin_id = response.body().getData().getAdmin_id();
                                              Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_SHORT).show();
                                              if (admin_id != null) {
                                                  socketIOManager.emit(SocketIOManager.MESSAGE_TYPE.LEFTUSER, setMessage(userId + " left from the group and admin is changed to " + admin_id, admin_id, destination_id, userId));
                                              } else {
                                                  socketIOManager.emit(SocketIOManager.MESSAGE_TYPE.LEFTUSER, setMessage(userId + " left from the group", admin, destination_id, userId));
                                              }

                                          } catch (Exception e) {
                                              e.printStackTrace();
                                          }

                                          if (userList.size() > 0) {
                                              userList.remove(position);
                                              mAdapter.notifyDataSetChanged();
                                          }

                                          //   socketIOManager.emit(SocketIOManager.MESSAGE_TYPE.LEFTUSER, setMessage(admin, destination_id, userId));

                                      }
                                      if (response.body().getStatus().equals(Constants.FAIL)) {
                                          CommonUtils.showValidationDialog(GroupDetailActivity.this, response.body().getErrorData());
                                      }
                                  } else if (response.errorBody() != null) {
                                      Log.e("Response", "response.errorBody() ");
                                  }
                              }

                              @Override
                              public void onFailure(Call<DeleteGroupResponse> call, Throwable t) {
                                  t.printStackTrace();
                                  mProgDialog.dismiss();
                                  CommonUtils.showValidationDialog(GroupDetailActivity.this, getString(R.string.server_error));
                              }
                          }

            );
        }
    }

    private JSONObject setMessage(String message, String adminId, long destination_id, int userId) {
        // handling typing events
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("destination_id", destination_id);
            jsonObject.put("user_id", userId);
            jsonObject.put("message", message);
            jsonObject.put("adminId", adminId);
            jsonObject.put("messageType", 3);
            jsonObject.put("destination_type", 1);
            jsonObject.put("created_on", System.currentTimeMillis());
            Log.e("USER DATA", jsonObject.toString());


            return jsonObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }


    public void setUserData(List<GroupUser> _userList) {
        this.userList = _userList;
        mAdapter.clear();
        mAdapter.addNewList(userList);

    }

    public void getGroupDetails() {
        if (CommonUtils.checkInternetConnection(this)) {
            GroupDetailRequest request = new GroupDetailRequest(destination_id);
            mProgDialog = ProgressDialog.show(GroupDetailActivity.this, "",
                    getString(R.string.please_wait), true);
            mProgDialog.setCancelable(false);
            mProgDialog.isIndeterminate();
            Call<GroupDetailsResponse> wsOTP = ((ChatLiveApplication) getApplicationContext())
                    .mApiCall.getGroupDetails(request, "Bearer " + userData.getAuthToken());
            wsOTP.enqueue(new Callback<GroupDetailsResponse>() {
                @Override
                public void onResponse(Call<GroupDetailsResponse> call, Response<GroupDetailsResponse> response) {
                    mProgDialog.dismiss();
                    if (response.body() != null) {
                        if (response.body().getStatus().equalsIgnoreCase(Constants.SUCCESS)) {
                            userList.clear();
                            userList = response.body().getData();
                            setUserData(userList);
                        }
                        if (response.body().getStatus().equals(Constants.FAIL)) {
                            CommonUtils.showValidationDialog(GroupDetailActivity.this, response.body().getErrorData());
                        }
                    } else if (response.errorBody() != null) {
                        Log.e("Response", "response.errorBody() ");
                    }
                }

                @Override
                public void onFailure(Call<GroupDetailsResponse> call, Throwable t) {
                    t.printStackTrace();
                    mProgDialog.dismiss();
                    CommonUtils.showValidationDialog(GroupDetailActivity.this, getString(R.string.server_error));
                }
            });
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        int userId = userList.get(position).getUserId();
        String isAdmin = userList.get(position).getIsAdmin();
        if (userList.size() > 1) {
            if (isAdmin.equalsIgnoreCase("1") || userData.getUserId() == Integer.parseInt(admin)) {
                deleteGroupUser(userId, position);
            } else {
                Toast.makeText(getApplicationContext(), "Only admin can delete the group", Toast.LENGTH_SHORT).show();
            }
        } else {
            deleteGroup(destination_id);
        }

    }

    // Admin can delete the group [webservice for delete group]
    public void deleteGroup(final long destination_id) {
        if (CommonUtils.checkInternetConnection(this)) {
            DeleteGroup request = new DeleteGroup(destination_id);
            mProgDialog = ProgressDialog.show(GroupDetailActivity.this, "",
                    getString(R.string.please_wait), true);
            mProgDialog.setCancelable(false);
            mProgDialog.isIndeterminate();
            Call<DeleteGroupResponse> wsOTP = ((ChatLiveApplication) getApplicationContext())
                    .mApiCall.deleteGroup(request, "Bearer " + userData.getAuthToken());
            wsOTP.enqueue(new Callback<DeleteGroupResponse>() {
                              @Override
                              public void onResponse(Call<DeleteGroupResponse> call, Response<DeleteGroupResponse> response) {
                                  mProgDialog.dismiss();
                                  if (response.body() != null) {
                                      SocketIOManager socketIOManager = SocketIOManager.getInstance();
                                      if (response.body().getStatus().equalsIgnoreCase(Constants.SUCCESS)) {
                                          try {
                                              String message = response.body().getData().getMessage();
                                              DatabaseHandler db = new DatabaseHandler(GroupDetailActivity.this);
                                              if (db.chatExist(destination_id) != 0) {
                                                  db.deleteChat(destination_id);
                                              }
                                              Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                              Intent intent = new Intent(GroupDetailActivity.this, MainActivity.class);
                                              intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                              startActivity(intent);
                                              finish();
                                          } catch (Exception e) {
                                              e.printStackTrace();
                                          }
                                      }
                                      if (response.body().getStatus().equals(Constants.FAIL)) {
                                          CommonUtils.showValidationDialog(GroupDetailActivity.this, response.body().getErrorData());
                                      }
                                  } else if (response.errorBody() != null) {
                                      Log.e("Response", "response.errorBody() ");
                                  }
                              }

                              @Override
                              public void onFailure(Call<DeleteGroupResponse> call, Throwable t) {
                                  t.printStackTrace();
                                  mProgDialog.dismiss();
                                  CommonUtils.showValidationDialog(GroupDetailActivity.this, getString(R.string.server_error));
                              }
                          }
            );
        }
    }

    private BroadcastReceiver userReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ChatMessage message = (ChatMessage) intent.getExtras().getSerializable(BundleKeys.PING_BUNDLE);
            DatabaseHandler db = new DatabaseHandler(GroupDetailActivity.this);
            String type = intent.getStringExtra(BundleKeys.PING_TYPE_BUNDLE);
            if (type.equals(SocketIOManager.MESSAGE_TYPE.LEFTUSER) && message.getMessage() != null) {
                if (db.chatExist(message.getToUserId()) != 0) {
                    int chatId = db.chatExist(message.getToUserId());
                    db.addMessage(chatId, message);
                } else {
                    db.addRecentChatUser(message.getToUserId(), "", message.getDestination_type(), 0);
                    int chatId = db.chatExist(message.getToUserId());
                    db.addMessage(chatId, message);
                }
            }
        }
    };
}
