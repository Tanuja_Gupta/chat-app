package com.chatlive.ui;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.chatlive.R;
import com.chatlive.data.ChatMessage;
import com.chatlive.database.DatabaseHandler;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi.DriveContentsResult;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveFolder.DriveFileResult;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.MetadataChangeSet;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;

public class GoogleDriveSaveDbActivity extends AppCompatActivity implements ConnectionCallbacks, OnConnectionFailedListener {

    private static final String TAG = "GoogleDrive";
    private GoogleApiClient api;
    private boolean mResolvingError = false;
    private DriveFile mfile;
    private static final int DIALOG_ERROR_CODE = 100;
    //    private static final String DATABASE_NAME = "person.db";
    private Button btn_Uplaod;
    private Button btn_getbackup;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_drive_save_db);
        btn_Uplaod = (Button) findViewById(R.id.btn_savebackup);
        btn_getbackup = (Button) findViewById(R.id.btn_getbackup);

        btn_getbackup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDriveFile(DatabaseHandler.DATABASE_NAME + ".db", "application/x-sqlite3");
            }

        });
        btn_Uplaod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseHandler db = new DatabaseHandler(GoogleDriveSaveDbActivity.this);
//                for (int i = 0; i < 4; i++) {
//                    ChatMessage chatMess = new ChatMessage();
//                    chatMess.setFromUserId(12345);
//                    chatMess.setToUserId(12345);
//                    chatMess.setMessage("Hello" + i);
//                    db.addMessage(chatMess);
//                }
                Drive.DriveApi.newDriveContents(api)
                        .setResultCallback(driveContentsCallback);
                /// CREATE OUR OWN FOLDER ON GOOGLE DRIVE
//                MetadataChangeSet changeSet = new MetadataChangeSet.Builder()
//                        .setTitle("Chatlive").build();
//                Drive.DriveApi.getRootFolder(api).createFolder(
//                        api, changeSet).setResultCallback(folderCreatedCallback);

            }
        });

//        // Create the Drive API instance
        api = new GoogleApiClient.Builder(this).
                addApi(Drive.API).
                addScope(Drive.SCOPE_FILE).
                addConnectionCallbacks(this).
                addOnConnectionFailedListener(this).build();
    }


    //Callback from google drive after creating foldetr
    ResultCallback<DriveFolder.DriveFolderResult> folderCreatedCallback = new
            ResultCallback<DriveFolder.DriveFolderResult>() {
                @Override
                public void onResult(DriveFolder.DriveFolderResult result) {
                    if (!result.getStatus().isSuccess()) {
                        return;
                    }

                    DriveFolder folder = result.getDriveFolder();//driveId.asDriveFolder();

                    Log.e("Created a folder: ", "" + result.getDriveFolder().getDriveId());
                }
            };

    @Override
    public void onStart() {
        super.onStart();
        if (!mResolvingError) {
            api.connect(); // Connect the client to Google Drive
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        api.disconnect(); // Disconnect the client from Google Drive
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (mResolvingError) { // If already in resolution state, just return.
            return;
        } else if (result.hasResolution()) { // Error can be resolved by starting an intent with user interaction
            mResolvingError = true;
            try {
                result.startResolutionForResult(this, DIALOG_ERROR_CODE);
            } catch (SendIntentException e) {
                e.printStackTrace();
            }
        } else { // Error cannot be resolved. Display Error Dialog stating the reason if possible.
            ErrorDialogFragment fragment = new ErrorDialogFragment();
            Bundle args = new Bundle();
            args.putInt("error", result.getErrorCode());
            fragment.setArguments(args);
            fragment.show(getFragmentManager(), "errordialog");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == DIALOG_ERROR_CODE) {
            mResolvingError = false;
            if (resultCode == RESULT_OK) { // Error was resolved, now connect to the client if not done so.
                if (!api.isConnecting() && !api.isConnected()) {
                    api.connect();
                }
            }
        }
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        Log.v(TAG, "Connected successfully");
        Toast.makeText(getApplicationContext(), "Connected", Toast.LENGTH_LONG).show();
    }

    // [START drive_contents_callback]
    final private ResultCallback<DriveContentsResult> driveContentsCallback =
            new ResultCallback<DriveContentsResult>() {
                @Override
                public void onResult(DriveContentsResult result) {
                    if (!result.getStatus().isSuccess()) {
                        return;
                    }

                    File databasePath = getApplicationContext().getDatabasePath(DatabaseHandler.DATABASE_NAME);
                    if (databasePath != null) {
                        upload(DatabaseHandler.DATABASE_NAME + ".db", databasePath, "application/x-sqlite3");
                    }
                }
            };

    // [END drive_contents_callback]
    @Override
    public void onConnectionSuspended(int cause) {
        // TODO Auto-generated method stub
        Log.v(TAG, "Connection suspended");

    }

    public void onDialogDismissed() {
        mResolvingError = false;
    }

    public static class ErrorDialogFragment extends DialogFragment {
        public ErrorDialogFragment() {
        }

        public Dialog onCreateDialog(Bundle savedInstanceState) {
            int errorCode = this.getArguments().getInt("error");
            return GooglePlayServicesUtil.getErrorDialog(errorCode, this.getActivity(), DIALOG_ERROR_CODE);
        }

        public void onDismiss(DialogInterface dialog) {
            ((GoogleDriveSaveDbActivity) getActivity()).onDialogDismissed();
        }
    }

    ///...
    void upload(final String titl, final File file, final String mime) {
        if (api != null && api.isConnected() && titl != null && file != null) try {
            Drive.DriveApi.newDriveContents(api).setResultCallback(new ResultCallback<DriveContentsResult>() {
                @Override
                public void onResult(@NonNull DriveContentsResult contRslt) {
                    if (contRslt.getStatus().isSuccess()) {
                        DriveContents cont = contRslt.getDriveContents();
                        if (cont != null && file2Os(cont.getOutputStream(), file)) {
                            MetadataChangeSet meta = new MetadataChangeSet.Builder().setTitle(titl).setMimeType(mime).build();
                            Drive.DriveApi.getAppFolder(api).createFile(api, meta, cont).setResultCallback(
                                    new ResultCallback<DriveFileResult>() {
                                        @Override
                                        public void onResult(@NonNull DriveFileResult fileRslt) {
                                            if (fileRslt.getStatus().isSuccess()) {
                                                Log.e("FILE", " " + fileRslt.getDriveFile().getDriveId());
                                                // fileRslt.getDriveFile();   BINGO !!!
                                            } else {
                                                Log.e("FILE", " Error");
                                            }
                                        }
                                    });
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static boolean file2Os(OutputStream os, File file) {
        boolean bOK = false;
        InputStream is = null;
        if (file != null && os != null) try {
            byte[] buf = new byte[4096];
            is = new FileInputStream(file);
            int c;
            while ((c = is.read(buf, 0, buf.length)) > 0)
                os.write(buf, 0, c);
            bOK = true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                os.flush();
                os.close();
                if (is != null) is.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return bOK;
    }

    /*******************************************************************
     * get file contents
     */
    private void getDriveFile(String title, String mime) {
        DriveId mDriveId = null;
        // content's COOL, create metadata
        MetadataChangeSet meta = new MetadataChangeSet.Builder().setTitle(title).setMimeType(mime).build();
        byte[] buf = null;
        if (api != null && api.isConnected()) try {
            DriveFile df = Drive.DriveApi.getFile(api, mDriveId);
            df.open(api, DriveFile.MODE_READ_ONLY, null)
                    .setResultCallback(new ResultCallback<DriveContentsResult>() {
                        @Override
                        public void onResult(DriveContentsResult driveContentsResult) {
                            if ((driveContentsResult != null) && driveContentsResult.getStatus().isSuccess()) {
                                DriveContents cont = driveContentsResult.getDriveContents();
                                // DUMP cont.getInputStream() to your DB file
                                cont.discard(api);    // or cont.commit();  they are equiv if READONLY
                            }
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    // get all Images

//    public void getImages(){
//        String pageToken = null;
//        do {
//            FileList result = driveService.files().list()
//                    .setQ("mimeType='image/jpeg'")
//                    .setSpaces("drive")
//                    .setFields("nextPageToken, files(id, name)")
//                    .setPageToken(pageToken)
//                    .execute();
//            for(File file: result.getFiles()) {
//                System.out.printf("Found file: %s (%s)\n",
//                        file.getName(), file.getId());
//            }
//            pageToken = result.getNextPageToken();
//        } while (pageToken != null);
//    }
}