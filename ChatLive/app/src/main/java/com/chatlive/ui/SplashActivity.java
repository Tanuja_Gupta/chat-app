package com.chatlive.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.chatlive.R;
import com.chatlive.commonutils.Constants;
import com.chatlive.commonutils.SessionManager;
import com.chatlive.responsedata.UserData;

/**
 * Created by Intex pc on 2/9/2017.
 */

public class SplashActivity extends AppCompatActivity {
    private UserData userData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
         /* New Handler to start the Menu-Activity
         * and close this Splash-Screen after some seconds.*/
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                userData = SessionManager.getSavedObjectFromPreference(SplashActivity.this,
                        Constants.SHAREDPREFRENCE,
                        Constants.USERDATA, UserData.class);
                if (userData == null) {
                    Intent i = new Intent(SplashActivity.this, RegistrationActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    if (userData.getAuthToken() != null) {
                        if (userData.getAuthToken().length() == 0) {
                            Intent i = new Intent(SplashActivity.this, OTPActivity.class);
                            startActivity(i);
                            finish();
                        } else {
                            Intent i = new Intent(SplashActivity.this, MainActivity.class);
                            startActivity(i);
                            finish();
                        }
                    }

                }
            }
        }, 3000);
    }
}
