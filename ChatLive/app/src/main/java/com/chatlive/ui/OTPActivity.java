package com.chatlive.ui;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountManager;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.chatlive.ChatLiveApplication;
import com.chatlive.R;
import com.chatlive.commonutils.CommonUtils;
import com.chatlive.commonutils.Constants;
import com.chatlive.commonutils.SessionManager;
import com.chatlive.responsedata.OTPresquest;
import com.chatlive.responsedata.UserData;
import com.chatlive.responsedata.UserResponse;
import com.google.firebase.iid.FirebaseInstanceId;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OTPActivity extends AccountAuthenticatorActivity implements View.OnClickListener {
    EditText et_OTP;
    private UserData userData;
    private ProgressDialog mProgDialog;
    private AccountManager mAccountManager;
    private static final int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 124;
    private String gcmId;
    // private String deviceToken;
    private String deviceId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        initView();
        userData = SessionManager.getSavedObjectFromPreference(OTPActivity.this,
                Constants.SHAREDPREFRENCE,
                Constants.USERDATA, UserData.class);
        mAccountManager = AccountManager.get(OTPActivity.this);
        gcmId = FirebaseInstanceId.getInstance().getToken();
        boolean result = checkPermissionPhoneState(this);
        if (result) {
            deviceId = CommonUtils.getIMEI(this);
            Log.e("FCM ID", "" + gcmId);
        }
    }

    private void initView() {
        et_OTP = (EditText) findViewById(R.id.et_Otp);
        findViewById(R.id.btn_submit).setOnClickListener(this);
        findViewById(R.id.btn_createAccount).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch ((v.getId())) {
            case R.id.btn_submit:
                if (validateOtp()) {
                    // Call for OTP webservice
                    callOtpWS();
                }
            case R.id.btn_createAccount: {
                //createAccount();
            }
            break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private boolean validateOtp() {
        String otp = et_OTP.getText().toString();
        if (otp.length() <= 0) {
            Toast.makeText(getApplicationContext(), "Enter Otp", Toast.LENGTH_LONG).show();
            return false;
        } else if (!otp.equals(userData.getPassword())) {
            Toast.makeText(getApplicationContext(), getString(R.string.otpError), Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
// Webservice call for  validating User OTP
    public void callOtpWS() {
        if (CommonUtils.checkInternetConnection(this)) {
            OTPresquest request = new OTPresquest(userData.getUserId(), et_OTP.getText().toString(), gcmId, "", deviceId);
            mProgDialog = ProgressDialog.show(OTPActivity.this, "",
                    getString(R.string.please_wait), true);
            mProgDialog.setCancelable(false);
            mProgDialog.isIndeterminate();
            Call<UserResponse> wsOTP = ((ChatLiveApplication) getApplicationContext())
                    .mApiCall.userOtp(request);
            wsOTP.enqueue(new Callback<UserResponse>() {
                @Override
                public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                    mProgDialog.dismiss();
                    if (response.body() != null) {
                        AddDeviceAccount addAccount = new AddDeviceAccount();
                        addAccount.execute(userData.getPhoneNumber(), et_OTP.getText().toString());
                        SessionManager.saveUserToSharedPreference(getApplicationContext(), Constants.SHAREDPREFRENCE, "userData", response.body().getData());
                    } else if (response.errorBody() != null) {
                        Log.e("Response", "response.errorBody() ");
                    }
                }

                @Override
                public void onFailure(Call<UserResponse> call, Throwable t) {
                    mProgDialog.dismiss();
                    CommonUtils.showValidationDialog(OTPActivity.this, getString(R.string.server_error));
                }
            });
        }
    }


//     Run time permission check
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_PHONE_STATE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    deviceId = CommonUtils.getIMEI(this);
                } else {
                    finish();
                }
                break;
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static boolean checkPermissionPhoneState(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_PHONE_STATE)) {
                    android.support.v7.app.AlertDialog.Builder alertBuilder = new android.support.v7.app.AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("Read phone state permission is necessary");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_PHONE_STATE}, MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
                        }
                    });
                    android.support.v7.app.AlertDialog alert = alertBuilder.create();
                    alert.show();

                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_PHONE_STATE}, MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }


    // adding account to device Account manager
    public class AddDeviceAccount extends AsyncTask<String, Void, Boolean> {
        ProgressDialog mProgDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgDialog = ProgressDialog.show(OTPActivity.this, "",
                    getString(R.string.please_wait), true);
            mProgDialog.setCancelable(false);
            mProgDialog.isIndeterminate();
        }

        @Override
        public Boolean doInBackground(String... params) {
            String user = params[0];
            String pass = params[1];
            Bundle result = null;
            Account account = new Account(user, OTPActivity.this.getString(R.string.ACCOUNT_TYPE));
            AccountManager am = AccountManager.get(OTPActivity.this);
            if (am.addAccountExplicitly(account, pass, null)) {
                result = new Bundle();
                result.putString(AccountManager.KEY_ACCOUNT_NAME, account.name);
                result.putString(AccountManager.KEY_ACCOUNT_TYPE, account.type);
                setAccountAuthenticatorResult(result);
                return true;
            } else {
                return false;
            }
        }

        @Override
        public void onPostExecute(Boolean result) {
            if (mProgDialog.isShowing()) {
                mProgDialog.dismiss();
            }
            Intent i = new Intent(OTPActivity.this, ProfileActivity.class);
            startActivity(i);
        }
    }


}
