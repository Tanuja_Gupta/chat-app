package com.chatlive.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.chatlive.Adapters.CodeAdapter;
import com.chatlive.ChatLiveApplication;
import com.chatlive.R;
import com.chatlive.commonutils.CommonUtils;
import com.chatlive.commonutils.Constants;
import com.chatlive.commonutils.SessionManager;
import com.chatlive.data.SpinnerViewList;
import com.chatlive.responsedata.RegisterRequest;
import com.chatlive.responsedata.UserData;
import com.chatlive.responsedata.UserResponse;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RegistrationActivity extends AppCompatActivity implements View.OnClickListener {
    private Spinner spnr_countryCode;
    private ArrayList<SpinnerViewList> codeList = new ArrayList<>();
    private String countryCode;
    private String countryISO;
    private EditText et_phone;
    private ProgressDialog mProgDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        //getting Ids of the view
        initView();
        // Bind country codes
        bindCodeData();
        UserData userData = SessionManager.getSavedObjectFromPreference(RegistrationActivity.this,
                Constants.SHAREDPREFRENCE,
                Constants.USERDATA, UserData.class);
        if (userData != null) {
            if (userData.getUserId() != 0) {
                Intent intent = new Intent(RegistrationActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }

    }

    // Getting Id's of the Views used
    public void initView() {
        et_phone = (EditText) findViewById(R.id.et_phone);
        findViewById(R.id.btn_Submit).setOnClickListener(this);
        spnr_countryCode = (Spinner) findViewById(R.id.spnr_countryCode);
    }

    // Binding country code data to Spinner
    public void bindCodeData() {
        codeList = spinnerList("countrycode.json");
        sortArrayList();
        final CodeAdapter codeAdaptr = new CodeAdapter(RegistrationActivity.this, codeList);
        spnr_countryCode.setAdapter(codeAdaptr);
        spnr_countryCode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                countryCode = String.valueOf(codeAdaptr.getSpinnerCode(position));
                countryISO = codeAdaptr.getSpinnerISO(position);
                Log.e("COUNTRY ISO", countryISO + "---");
                spnr_countryCode.setSelection(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    // Getting country code data  from the file stored on assets
    public ArrayList<SpinnerViewList> spinnerList(String file_name) {
        ArrayList<SpinnerViewList> mList = new ArrayList<>();
        try {
            JSONArray jobjn = new JSONArray(CommonUtils.loadJSONFromAsset(RegistrationActivity.this, file_name));
            for (int i = 0; i < jobjn.length(); i++) {
                if (jobjn.getJSONObject(i).has("name")) {
                    String name = jobjn.getJSONObject(i).getString("name");
                    mList.add(new SpinnerViewList(i, name, jobjn.getJSONObject(i).getString("dial_code"), jobjn.getJSONObject(i).getString("code")));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return mList;
    }

    // Sorting country code data alphabetically in ascending order i.e A-Z
    public void sortArrayList() {
        //Sorting List alphabetically
        Collections.sort(codeList, new Comparator<SpinnerViewList>() {
            @Override
            public int compare(final SpinnerViewList object1, final SpinnerViewList object2) {
                return object1.getName().compareTo(object2.getName());
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_Submit:
                if (validatePhoneNumber()) {
                    // `Calling Resiter Api to get Resister on App
                    resisterUserWS();
                }
                break;
        }
    }

    // Validations for Phone number and country code before Resister or login
    private boolean validatePhoneNumber() {
        boolean isValidated = true;
        if (!CommonUtils.validatePhoneNumber(et_phone.getText().toString())) {
            isValidated = false;
            Toast.makeText(getApplicationContext(), "Please enter valid phone number", Toast.LENGTH_LONG).show();
        } else if (countryCode.length() <= 0) {
            isValidated = false;
            Toast.makeText(getApplicationContext(), "Please select valid country code", Toast.LENGTH_LONG).show();
        }
        return isValidated;
    }

    // API calf to get Resister on application
    public void resisterUserWS() {
        if (CommonUtils.checkInternetConnection(this)) {
            RegisterRequest request = new RegisterRequest(countryCode, et_phone.getText().toString(), countryISO);
            mProgDialog = ProgressDialog.show(RegistrationActivity.this, "",
                    getString(R.string.please_wait), true);
            mProgDialog.setCancelable(false);
            mProgDialog.isIndeterminate();
            Call<UserResponse> wsOTP = ((ChatLiveApplication) getApplicationContext())
                    .mApiCall.registerUser(request);
            wsOTP.enqueue(new Callback<UserResponse>() {
                @Override
                public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                    mProgDialog.dismiss();
                    if (response.body() != null) {
                        if (response.body().getStatus().equals(Constants.SUCCESS)) {
                            SessionManager.saveUserToSharedPreference(getApplicationContext(), Constants.SHAREDPREFRENCE, "userData", response.body().getData());
                            //Calling OTP activity to verify user
                            Intent i = new Intent(RegistrationActivity.this, OTPActivity.class);
                            startActivity(i);
                            finish();
                        }
                        if (response.body().getStatus().equals(Constants.FAIL)) {
                            String msg = null;
                            if (response.body().getErrorData() != null) {
                                switch (response.body().getErrorData()) {
                                    case "01":
                                        msg = Constants.Error1;
                                        break;
                                    case "02":
                                        msg = Constants.Error2;
                                        break;
                                    case "03":
                                        msg = Constants.Error3;
                                        break;
                                    case "04":
                                        msg = Constants.Error4;
                                        break;
                                    case "05":
                                        msg = Constants.Error5;
                                        break;
                                    case "06":
                                        msg = Constants.Error6;
                                        break;
                                }
                            }
                            CommonUtils.showValidationDialog(RegistrationActivity.this, msg);
                        }
                    } else if (response.errorBody() != null) {
                        Log.e("Response", "response.errorBody() ");
                    }
                }

                @Override
                public void onFailure(Call<UserResponse> call, Throwable t) {
                    mProgDialog.dismiss();
                    CommonUtils.showValidationDialog(RegistrationActivity.this, getString(R.string.server_error));
                }
            });
        }
    }
}

