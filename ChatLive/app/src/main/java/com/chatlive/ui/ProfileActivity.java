package com.chatlive.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.chatlive.ChatLiveApplication;
import com.chatlive.R;
import com.chatlive.commonutils.CommonUtils;
import com.chatlive.commonutils.Constants;
import com.chatlive.commonutils.SessionManager;
import com.chatlive.responsedata.UserData;
import com.chatlive.responsedata.UserResponse;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity {
    private Button btn_select;
    private ImageView img_Profile;
    private int REQUEST_CAMERA = 0;
    private int SELECT_FILE = 1;
    private String userChoosenTask;
    private ProgressDialog mProgDialog;
    private UserData userData;
    private EditText et_username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        userData = SessionManager.getSavedObjectFromPreference(ProfileActivity.this,
                Constants.SHAREDPREFRENCE,
                Constants.USERDATA, UserData.class);
        //setting up View
        initView();
        // Set user details shared on sharedprefrence
        setUserDetail();

    }

    // getting Ids for setting up view
    private void initView() {
        btn_select = (Button) findViewById(R.id.btn_Select);
        img_Profile = (ImageView) findViewById(R.id.img_Profile);
        et_username = (EditText) findViewById(R.id.et_username);
        btn_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // calling dialog for selecting image
                selectImage();
            }
        });
    }

    // Set User Details from sharedprefrence
    public void setUserDetail() {
        et_username.setText(userData.getName());
        if (userData.getAvatarImage() != null) {
            Picasso.with(ProfileActivity.this)
                    .load(userData.getAvatarImage())
                    .error(R.mipmap.ic_launcher)
                    .placeholder(R.mipmap.ic_launcher)
                    .into(img_Profile);
        }

    }

    // Checking permissions for Storage
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case CommonUtils.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Gallery"))
                        galleryIntent();
                } else {
                    finish();
                }
                break;
        }
    }

    // Dialog to select photo from camera and gallery
    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Gallery",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = CommonUtils.checkPermission(ProfileActivity.this);
                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        cameraIntent();
                } else if (items[item].equals("Choose from Gallery")) {
                    userChoosenTask = "Choose from Gallery";
                    if (result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    // Gallery intent for choosing image
    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select Image"), SELECT_FILE);
    }

    // Camera intent for choosing image
    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                onSelectFromGalleryResult(data);
            } else if (requestCode == REQUEST_CAMERA) {
                onCaptureImageResult(data);
            }
        }


    }

    // Getting image data from Bundle camera
    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
            if (destination.exists()) {
                updateUserWS(destination.getAbsolutePath());
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        img_Profile.setImageBitmap(thumbnail);

    }

    // getting image data from Gallery
    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                // change bitmap to byte array
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();
                // convert byte array to base64
                String base64 = Base64.encodeToString(byteArray, Base64.DEFAULT);
                Uri tempUri = CommonUtils.getImageUri(ProfileActivity.this, bm);
                String filepath = CommonUtils.getRealPathFromURI(getApplicationContext(), tempUri);
                // convert base64 to  byte array
                bm = CommonUtils.base64ToBitmap(base64);
                File destination = new File(filepath);
                if (destination.exists()) {
                    updateUserWS(filepath);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        img_Profile.setImageBitmap(bm);
    }

    // API call to get Resister on application
    public void updateUserWS(String path) {
        MultipartBody.Part imagePath = null;
        if (path != null) {
            File file = new File(path);
            // create RequestBody instance from file
            RequestBody requestFile =
                    RequestBody.create(MediaType.parse("multipart/form-data"), file);
            imagePath = MultipartBody.Part.createFormData("avatarImage", file.getName(), requestFile);
        }
        // adding another part within the multipart request for rest of fields
        RequestBody userID = CommonUtils.getRequestBody(String.valueOf(userData.getUserId()));
        RequestBody profileName = CommonUtils.getRequestBody(et_username.getText().toString());
        RequestBody profileImage = CommonUtils.getRequestBody("profileImg_" + userData.getUserId());
        if (CommonUtils.checkInternetConnection(this)) {
            mProgDialog = ProgressDialog.show(ProfileActivity.this, "",
                    getString(R.string.please_wait), true);
            mProgDialog.setCancelable(false);
            mProgDialog.isIndeterminate();
            Call<UserResponse> wsOTP = ((ChatLiveApplication) getApplicationContext())
                    .mApiCall.updateUser(userID, profileName, profileImage, imagePath);
            wsOTP.enqueue(new Callback<UserResponse>() {
                @Override
                public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                    mProgDialog.dismiss();
                    if (response.body() != null) {
                        Toast.makeText(getApplicationContext(), "success", Toast.LENGTH_LONG).show();
                        if (response.body().getStatus().equals(Constants.SUCCESS)) {
                            SessionManager.saveUserToSharedPreference(getApplicationContext(), Constants.SHAREDPREFRENCE, "userData", response.body().getData());
                            Intent i = new Intent(ProfileActivity.this, MainActivity.class);
                            startActivity(i);
                            finish();

                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<UserResponse> call, Throwable t) {
                    mProgDialog.dismiss();
                    CommonUtils.showValidationDialog(ProfileActivity.this, getString(R.string.server_error));
                }
            });
        }
    }
}
