//package com.chatlive;
//
//import android.animation.Animator;
//import android.app.Activity;
//import android.app.NotificationManager;
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.graphics.Paint;
//import android.os.Bundle;
//import android.text.Editable;
//import android.text.TextWatcher;
//import android.view.View;
//import android.view.inputmethod.InputMethodManager;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.LinearLayout;
//import android.widget.ListView;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//
//import com.android.volley.VolleyError;
//import com.test.dok.network.HttpDataClient;
//import com.test.dok.utils.Constants;
//import com.test.dok.utils.NetworkUtil;
//import com.test.dok.utils.Utils;
//import com.test.dok.vo.Chat;
//import com.test.dok.vo.ChatMessage;
//import com.test.dok.vo.ChatResult;
//import com.test.dok.vo.SessionContext;
//
//
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.Iterator;
//import java.util.List;
//import java.util.Timer;
//import java.util.TimerTask;
//
//import butterknife.Bind;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
//
//
//public class ChatMessageActivity extends ParentActivity {
//
//    private List<ChatMessage> chatMessageList = new ArrayList<>();
//    private ChatMessageAdapter adapter;
//    private Chat chat;
//    private ChatDataSource chatDataSource;
//    private int statusBarOffset;
//    private int chatOptionOffset;
//    private SocketIOManager socketIOManager;
//    private boolean isEventAlreadyLog = false;
//
//    public static int exchangeChatId = 0;
//
//    @Bind(R.id.chatMessageList)
//    ListView chatMessageListView;
//
//    @Bind(R.id.chatMessageFirstAmount)
//    TextView chatMessageFirstAmount;
//
//    @Bind(R.id.chatMessageSecondAmount)
//    TextView chatMessageSecondAmount;
//
//    @Bind(R.id.chatMessageEditText)
//    EditText chatMessageEditText;
//
//    @Bind(R.id.chatMessageSendBtn)
//    Button chatMessageSendBtn;
//
//    @Bind(R.id.chatMessageOptionsTransparentBG)
//    RelativeLayout chatMessageOptionsTransparentBG;
//
//    @Bind(R.id.chatMessageOptionLayout)
//    LinearLayout chatMessageOptionLayout;
//
//    @Bind(R.id.chatMessageRootLayout)
//    RelativeLayout chatMessageRootLayout;
//
//    @Bind(R.id.chatMessageFreezeExchangeBtn)
//    Button chatMessageFreezeExchangeBtn;
//
//    private HttpDataClient<ChatResult> chatResultHttpDataClient;
//    private TextView actionvarTitleTV;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_chat_message);
//        ButterKnife.bind(this);
//        chatMessageRootLayout.post(new Runnable() {
//            @Override
//            public void run() {
//                statusBarOffset = getResources().getDisplayMetrics().heightPixels - chatMessageRootLayout.getMeasuredHeight();
//                chatOptionOffset = chatMessageOptionLayout.getMeasuredHeight();
//            }
//        });
//        chatDataSource = new ChatDataSource();
//        chatDataSource.open();
//        chat = (Chat) getDataBundle().getSerializable(BundleKeys.CHAT_MESSAGE_DETAIL_BUNDLE);
//
//        if (chat == null) {
//            exchangeChatId = getDataBundle().getInt(BundleKeys.EXCHANGE_CHAT_ID_BUNDLE);
//            getChat(exchangeChatId);
//        } else {
//            initChatScreen();
//        }
//
//        socketIOManager = SocketIOManager.getInstance();
//
//        IntentFilter chatBroadcastIntentFilter = new IntentFilter();
//        chatBroadcastIntentFilter.addAction(Constants.PING_CONSTANTS.PING_ACTION);
//        registerReceiver(chatReceiver, chatBroadcastIntentFilter);
//
//        IntentFilter networkBroadcastIntentFilter = new IntentFilter();
//        networkBroadcastIntentFilter.addAction(Constants.NETWORK_CHANGE.CONNECTIVITY_CHANGE);
//        registerReceiver(mNetworkBroadcastReceiver, networkBroadcastIntentFilter);
//
//
//        NotificationManager mNotificationManager =
//                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//        mNotificationManager.cancel(Constants.PING_CONSTANTS.PING_NOTIFICATION_ID);
//
//        //updateExchangeStatusResultHttpDataClient = getHttpClient(UpdateExchangeStatusResult.class);
//        //updateExchangeStatusResultHttpDataClient.setOnHttpDataListener(onUpdateStatus);
//
//    }
//
//
//    private void initChatScreen() {
//        getChatData();
//    }
//
//
//    private void getChat(int exchangeChatId) {
//        chatResultHttpDataClient = getHttpClient(ChatResult.class);
//        chatResultHttpDataClient.setOnHttpDataListener(chatResultHttpDataListener);
//        chatResultHttpDataClient.addParam("exchangeChatId", exchangeChatId);
//        chatResultHttpDataClient.addHeader("Authorization", "Bearer " + getSessionContext().getUser().getAppAccessToken());
//        chatResultHttpDataClient.executePost(Constants.URL.GET_CHATS_URL);
//    }
//
//    private HttpDataListener<ChatResult> chatResultHttpDataListener = new HttpDataListener<ChatResult>() {
//        @Override
//        public void onHttpData(ChatResult data) {
//            chat = data.getData();
//            initChatScreen();
//        }
//
//        @Override
//        public void onHttpError(VolleyError error) {
//            getChat(exchangeChatId);
//        }
//    };
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        if (chat != null) {
//            exchangeChatId = chat.getExchangeChatId();
//        }
//    }
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        exchangeChatId = 0;
//    }
//
//
//    private BroadcastReceiver chatReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            ChatMessage message = (ChatMessage) intent.getExtras().getSerializable(BundleKeys.PING_BUNDLE);
//            String type = intent.getStringExtra(BundleKeys.PING_TYPE_BUNDLE);
//            // if chat is for other user
//            if ((type.equals(SocketIOManager.MESSAGE_TYPE.PING) || type.equals(SocketIOManager.MESSAGE_TYPE.TYPE_START) ||
//                    type.equals(SocketIOManager.MESSAGE_TYPE.TYPE_END))
//                    && message.getExchangeChatId() != chat.getExchangeChatId())
//                return;
//
//
//            Date now = new Date();
//            if (type.equals(SocketIOManager.MESSAGE_TYPE.PING)) {
//                chatDataSource.markAllChatRead(chat.getExchangeChatId());
//                SimpleDateFormat formatter = new SimpleDateFormat("MMM d");
//                if (chatMessageList.size() > 0 && Utils.getLocalDate(chatMessageList.get(chatMessageList.size() - 1).getCreatedOn(), "MMM d").equalsIgnoreCase(formatter.format(now))) {
//                    message.setIsSeparator(false);
//                } else {
//                    ChatMessage seperator = new ChatMessage();
//                    seperator.setIsSeparator(true);
//                    seperator.setCreatedOn(Utils.formatToUTC(now));
//                    adapter.addMessage(seperator);
//                }
//                adapter.addMessage(message);
//            } else if (type.equals(SocketIOManager.MESSAGE_TYPE.TYPE_START)) {
//                ChatMessage typing = new ChatMessage();
//                typing.setIsTyping(true);
//                typing.setCreatedOn(Utils.formatToUTC(now));
//                adapter.addMessage(typing);
//            } else if (type.equals(SocketIOManager.MESSAGE_TYPE.TYPE_END)) {
//                adapter.popMessage();
//            } else if (type.equals(SocketIOManager.MESSAGE_TYPE.RECONNECT)) {
//                if (!chatMessageEditText.getText().toString().trim().equalsIgnoreCase("")) {
//                    chatMessageSendBtn.setEnabled(true);
//                }
//                //do nothing
//            } else if (type.equals(SocketIOManager.MESSAGE_TYPE.DISCONNECT)) {
//                chatMessageSendBtn.setEnabled(false);
//                //do nothing
//            }
//        }
//    };
//
//
//    private void getChatData() {
//        chatMessageList = chatDataSource.getChatMessages(chat.getExchangeChatId());
//        if (chatMessageList.size() == 0) {
//            chatMessageEditText.setText(getResources().getString(R.string.whatsup_txt));
//            chatMessageSendBtn.setEnabled(true);
//        }
//        for (int k = 0; k < chatMessageList.size(); k++) {
//            if (isEventAlreadyLog) {
//                break;
//            }
//            if (((ChatMessage) chatMessageList.get(k)).getToUserId() != getSessionContext().getUser().getUserId()) { // set userId
//                isEventAlreadyLog = true;
//                break;
//            }
//        }
//        adapter = new ChatMessageAdapter(this, chatMessageList, chat.getWithUser());
//        chatMessageListView.setAdapter(adapter);
//        chatDataSource.markAllChatRead(chat.getExchangeChatId());
//        chatMessageEditText.addTextChangedListener(messageWatcher);
//        //send update chat count broadcast
//        Intent updateChatCountIntent = new Intent();
//        updateChatCountIntent.setAction(Constants.PING_CONSTANTS.UPDATE_CHAT_COUNT_ACTION);
//        sendBroadcast(updateChatCountIntent);
//
//    }
//
//    @OnClick(R.id.chatMessageSendBtn)
//    public void onSendBtnClick() {
//        Date now = new Date();
//        ChatMessage message = new ChatMessage();
//        message.setExchangeChatId(chat.getExchangeChatId());
//        message.setCreatedOn(Utils.formatToUTC(now));
//        message.setMessage(chatMessageEditText.getText().toString().trim());
//        message.setFromUserId(SessionContext.getInstance().getUser().getUserId());
//        message.setIsRead(true);
//        message.setIsSent(false);
//        message.setFromUserName(SessionContext.getInstance().getUser().getName());
//        message.setFromUserPic(SessionContext.getInstance().getUser().getAvatarImage());
//        SimpleDateFormat formatter = new SimpleDateFormat("MMM d");
//        if (chatMessageList.size() > 0 && Utils.getLocalDate(chatMessageList.get(chatMessageList.size() - 1).getCreatedOn(), "MMM d").equalsIgnoreCase(formatter.format(now))) {
//            message.setIsSeparator(false);
//        } else {
//            ChatMessage seperator = new ChatMessage();
//            seperator.setIsSeparator(true);
//            seperator.setCreatedOn(Utils.formatToUTC(now));
//            adapter.addMessage(seperator);
//        }
//        message.setToUserId(chat.getWithUser().getUserId());
//        long chatId = chatDataSource.insertChatMessage(message);
//        if (chatId != -1) {
//            message.setChatId((int) chatId);
//        }
//        if (chatMessageList.size() == 1) {
//            isEventAlreadyLog = true;
//        } else if (!isEventAlreadyLog) {
//            isEventAlreadyLog = true;
//        }
//        adapter.addMessage(message);
//        chatMessageEditText.setText("");
//        message.setIsRead(false);
//        socketIOManager.emit(SocketIOManager.MESSAGE_TYPE.PING, jsonUtil.toJson(message));
//
//    }
//
//
//    private Timer typingTimer = new Timer();
//    private final TextWatcher messageWatcher = new TextWatcher() {
//        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//        }
//
//        public void onTextChanged(CharSequence s, int start, int before, int count) {
//        }
//
//        @Override
//        public void afterTextChanged(Editable s) {
//            if (chatMessageEditText.getText().toString().trim().equalsIgnoreCase("") ||
//                    NetworkUtil.getConnectivityStatus(ChatMessageActivity.this) == NetworkUtil.TYPE_NOT_CONNECTED) {
//                chatMessageSendBtn.setEnabled(false);
//            } else {
//                chatMessageSendBtn.setEnabled(true);
//            }
//            socketIOManager.emit(SocketIOManager.MESSAGE_TYPE.TYPE_START, jsonUtil.toJson(setMessage()));
//            typingTimer.cancel();
//            typingTimer = new Timer();
//            typingTimer.schedule(new TimerTask() {
//                @Override
//                public void run() {
//                    socketIOManager.emit(SocketIOManager.MESSAGE_TYPE.TYPE_END, jsonUtil.toJson(setMessage()));
//                }
//            }, 1000);
//        }
//    };
//
//
//    private ChatMessage setMessage() {
//        Date now = new Date();
//        // handling typing events
//        ChatMessage typingMessage = new ChatMessage();
//        typingMessage.setToUserId(chat.getWithUser().getUserId());
//        typingMessage.setExchangeChatId(chat.getExchangeChatId());
//        typingMessage.setCreatedOn(Utils.formatToUTC(now));
//        typingMessage.setMessage("");
//        typingMessage.setFromUserId(SessionContext.getInstance().getUser().getUserId());
//        typingMessage.setIsRead(true);
//        typingMessage.setIsSent(false);
//        typingMessage.setFromUserName(SessionContext.getInstance().getUser().getName());
//        typingMessage.setFromUserPic(SessionContext.getInstance().getUser().getAvatarImage());
//
//        return typingMessage;
//    }
//
//    private void openOptionSheet() {
//        //hideKeybord(chatMessageEditText);
//        chatMessageOptionsTransparentBG.setVisibility(View.VISIBLE);
//        chatMessageOptionLayout.setVisibility(View.VISIBLE);
//        chatMessageOptionLayout.setY(Constants.deviceHeight);
//        chatMessageOptionLayout.animate().y(Constants.deviceHeight - chatOptionOffset - statusBarOffset).setDuration(500).start();
//    }
//
//    private void closeOptionSheet() {
//        chatMessageOptionLayout.animate().y(Constants.deviceHeight).setDuration(500).setListener(new Animator.AnimatorListener() {
//            @Override
//            public void onAnimationStart(Animator animation) {
//
//            }
//
//            @Override
//            public void onAnimationEnd(Animator animation) {
//                chatMessageOptionsTransparentBG.setVisibility(View.GONE);
//                chatMessageOptionLayout.setVisibility(View.INVISIBLE);
//                chatMessageOptionLayout.animate().setListener(null);
//            }
//
//            @Override
//            public void onAnimationCancel(Animator animation) {
//
//            }
//
//            @Override
//            public void onAnimationRepeat(Animator animation) {
//
//            }
//        }).start();
//    }
//
//    //options handlings
//    @OnClick(R.id.chatMessageCancelBtn)
//    public void cancelOptions() {
//        closeOptionSheet();
//    }
//
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        unregisterReceiver(chatReceiver);
//        unregisterReceiver(mNetworkBroadcastReceiver);
//    }
//
//}
///*@Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode == RESULT_OK) {
//            Intent requestCodeIntent = new Intent();
//            if (requestCode != HomeActivity.RequestType.FREEZE_UNFREEZ) {
//                requestCodeIntent.putExtra(BundleKeys.REQUEST_CODE, requestCode);
//            }
//
//            setResult(RESULT_OK, requestCodeIntent);
//            finish();
//            overridePendingTransition(R.anim.hold, R.anim.push_out_to_bottom);
//        }
//    }*//*
//
//
//    public void hideKeybord(View view) {
//        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(),
//                InputMethodManager.RESULT_UNCHANGED_SHOWN);
//    }
//
//    private BroadcastReceiver mNetworkBroadcastReceiver = new BroadcastReceiver() {
//
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            if (NetworkUtil.getConnectivityStatus(context) == NetworkUtil.TYPE_NOT_CONNECTED) {
//                chatMessageSendBtn.setEnabled(false);
//            } else if (NetworkUtil.getConnectivityStatus(context) == NetworkUtil.TYPE_WIFI
//                    || NetworkUtil.getConnectivityStatus(context) == NetworkUtil.TYPE_MOBILE) {
//                if (!chatMessageEditText.getText().toString().equalsIgnoreCase("")) {
//                    chatMessageSendBtn.setEnabled(true);
//                    attemptPendingMessages();
//                }
//
//            }
//        }
//    };
//
//    private void attemptPendingMessages() {
//        List<ChatMessage> messages = chatDataSource.getUnsentChats(SessionContext.getInstance().getUser().getUserId());
//
//        Iterator<ChatMessage> iterator = messages.iterator();
//
//        while (iterator.hasNext()) {
//            ChatMessage message = iterator.next();
//            message.setIsRead(false);
//            socketIOManager.emit(SocketIOManager.MESSAGE_TYPE.PING, jsonUtil.toJson(message));
//
//        }
//    }
//
//}
