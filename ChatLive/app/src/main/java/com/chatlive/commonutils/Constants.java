package com.chatlive.commonutils;

import android.os.Environment;

import java.io.File;

/**
 * Created by admin on 30/01/2017.
 */

public class Constants {

    public interface URL {

        String DEV_SERVER = "http://192.168.1.15:5100";
        String BASE_URL = DEV_SERVER;
        String CHAT_SERVER_URL = BASE_URL;
        String GET_CHATS_URL = BASE_URL + "/api/a/getChat";
        String GET_ALL_CHATS_URL = BASE_URL + "/api/a/getAllChats";
        String LOGIN_URL = BASE_URL + "/api/V2/" + "login";
    }

    public interface PING_CONSTANTS {
        String PING_ACTION = "cash.uchange.android.PING_ACTION";
        String UPDATE_CHAT_COUNT_ACTION = "cash.uchange.android.UPDATE_CHAT_COUNT_ACTION";
        int PING_NOTIFICATION_ID = 1987;
    }

    public static String ACTION_FCMTOKEN = "token";
    public static String SUCCESS = "success";
    public static String FAIL = "fail";

    public static String Error1 = "USER NOT AUTHORISED WRONG TOKEN";
    public static String Error2 = "USER NOT AUTHORISED TOKEN MISSING";
    public static String Error3 = "PARAMETERS  MISSING";
    public static String Error4 = "PHONE  NUMBER  ALREADY  EXISTS";
    public static String Error5 = "THERE  IS  SOME  ERROR  PLEASE  TRY  AGAIN  LATER";
    public static String Error6 = "MISSING  DEVICE_ID";
    public static String SHAREDPREFRENCE = "CHATLIVE";
    public static String FILEPATH = Environment.getExternalStorageDirectory()
            + File.separator + "Chat Live" + File.separator;

    public static String GROUPID = "group_id";
    public static String GROUPADMIN = "groupAdmin";
    public static String USERDATA = "userData";
    public static String DESTINATIONID = "destination_id";
    public static String DESTINATIONTYPE = "destinationtype";
    public static String PHONENUM = "phoneNumber";

}
