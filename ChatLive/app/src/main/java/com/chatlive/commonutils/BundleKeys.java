package com.chatlive.commonutils;

public class BundleKeys {

	public static final String PING_BUNDLE = "PING_BUNDLE";
	public static final String PING_TYPE_BUNDLE = "PING_TYPE_BUNDLE";
	public static final String UPDATEUSER_BUNDLE = "userlist";
	public static final String FCMID = "fcmID";
}
