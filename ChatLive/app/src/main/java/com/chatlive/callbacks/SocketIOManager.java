package com.chatlive.callbacks;

import android.util.Log;


import com.chatlive.commonutils.Constants;
import com.chatlive.ui.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class SocketIOManager {
    public interface MESSAGE_TYPE {
        String CONNECTION = "connection";
        String CONNECTED = "connected";
        String PING = "ping";
        String PINGPASS = "pingpass";
        String PINGFAIL = "pingfail";
        String DISCONNECT = "disconnect";
        String RECONNECT = "reconnect";
        String TYPE_START = "typestart";
        String TYPE_END = "typeend";
        String HEARTBEAT = "heartbeat";
        String USERLIST = "userlist";
        String ADDUSER= "adduser";
        String LEFTUSER= "leftuser";
    }

    public boolean isConnected = false;


    private static SocketIOManager instance;
    private Socket socket;
    private Emitter emitter;
    private IO.Options options = new IO.Options();

    {
        try {
            options.reconnection = true;
            options.forceNew = false;
            options.reconnectionDelay = 2000;
            options.reconnectionDelayMax = 2000;
            socket = IO.socket(Constants.URL.CHAT_SERVER_URL, options);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public static SocketIOManager getInstance() {
        if (instance == null)
            instance = new SocketIOManager();

        return instance;
    }

    public void connect(String url, final SocketConnectionListener connectionListener) {
        /**
         * Changes from socket.on to socket.once
         */
        emitter = socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                JSONObject obj = new JSONObject();
                try {
                    obj.put("userId",MainActivity.userId);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                isConnected = true;
                connectionListener.onConnect();
                emit(MESSAGE_TYPE.CONNECTED, obj);
            }

        }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                isConnected = false;
                connectionListener.onDisconnect();
            }

        }).on(Socket.EVENT_RECONNECT, new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                if (!isConnected) {
                    socket.connect();
                    isConnected = true;
                    connectionListener.onReconnect();
                }
            }

        }).on(Socket.EVENT_CONNECT_TIMEOUT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.e("SocketIO", "connection timeout");
            }
        });
        socket.connect();
    }

    public void disconnect() {
        this.socket.off();
        socket.disconnect();
    }

    public void emit(String eventName, String data) {
        if (isConnected) {
            try {
                socket.emit(eventName, new JSONObject(data));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void emit(String eventName, JSONObject data) {
        if (isConnected) {
            socket.emit(eventName, data);
        }
    }

    public void listenForEvent(String eventName, final SocketEventCallback socketEventCallback) {
        emitter.on(eventName, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                /**
                 * Changes for checking instance of object.
                 */
                if (!(args[0] instanceof String)) {
                    socketEventCallback.onEventOccur((JSONObject) args[0]);
                }
            }
        });
    }

    public void destroyEvent(String eventName) {
        emitter.off(eventName);
    }
}
