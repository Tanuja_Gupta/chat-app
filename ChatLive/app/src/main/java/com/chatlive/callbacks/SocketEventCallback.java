package com.chatlive.callbacks;

import org.json.JSONObject;

public interface SocketEventCallback {
	public void onEventOccur(JSONObject obj);
}
