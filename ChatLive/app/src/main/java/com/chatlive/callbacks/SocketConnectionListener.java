package com.chatlive.callbacks;

public interface SocketConnectionListener {
	public void onConnect();
	public void onDisconnect();
	public void onReconnect();
}
