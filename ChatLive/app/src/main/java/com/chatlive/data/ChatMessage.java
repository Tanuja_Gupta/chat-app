package com.chatlive.data;

import com.chatlive.responsedata.GroupData;
import com.chatlive.responsedata.GroupUser;

import java.io.Serializable;
import java.util.List;

/**
 * Created by goyal.shubham on 21-10-2015.
 */
public class ChatMessage implements Serializable {
    private int exchangeChatId;
    private int chatId;
    private String message;
    private long created_on;
    private boolean isSeparator;
    private boolean isTyping;
    private boolean isRead;
    private long from_user_id;
    private long destination_id;
    private boolean isSent;
    private String fromUserName;
    private int messageType;
    private int destination_type;
    private String  thumb_path;
    private int  read_status;
    private int userId;
    private int mute;
    private String groupName;
    private List<GroupUser> userList;


    public int getMute() {
        return mute;
    }

    public void setMute(int mute) {
        this.mute = mute;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public int getUserId() {
        return userId;
    }

    public void setGroupList(List<GroupUser> userList) {
        this.userList = userList;
    }

    public List<GroupUser> getGroupList() {
        return userList;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getRead_status() {
        return read_status;
    }

    public void setRead_status(int read_status) {
        this.read_status = read_status;
    }

    public String getThumb_path() {
        return thumb_path;
    }

    public void setThumb_path(String thumb_path) {
        this.thumb_path = thumb_path;
    }

    public int getMessageType() {
        return messageType;
    }

    public void setMessageType(int messageType) {
        this.messageType = messageType;
    }

    public int getDestination_type() {
        return destination_type;
    }

    public void setDestination_type(int destination_type) {
        this.destination_type = destination_type;
    }

    private String fromUserPic;

    public ChatMessage() {
    }


    public boolean isSent() {
        return isSent;
    }

    public void setIsSent(boolean isSent) {
        this.isSent = isSent;
    }

    public int getChatId() {
        return chatId;
    }

    public void setChatId(int chatId) {
        this.chatId = chatId;
    }

    public long getCreatedOn() {
        return created_on;
    }

    public void setCreatedOn(long created_on) {
        this.created_on = created_on;
    }

    public int getExchangeChatId() {
        return exchangeChatId;
    }

    public void setExchangeChatId(int exchangeChatId) {
        this.exchangeChatId = exchangeChatId;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setIsRead(boolean isRead) {
        this.isRead = isRead;
    }

    public boolean isSeparator() {
        return isSeparator;
    }

    public void setIsSeparator(boolean isSeparator) {
        this.isSeparator = isSeparator;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getFromUserId() {
        return from_user_id;
    }

    public void setFromUserId(long from_user_id) {
        this.from_user_id = from_user_id;
    }

    public long getToUserId() {
        return destination_id;
    }

    public void setToUserId(long destination_id) {
        this.destination_id = destination_id;
    }

    public boolean isTyping() {
        return isTyping;
    }

    public void setIsTyping(boolean isTyping) {
        this.isTyping = isTyping;
    }

    public String getFromUserName() {
        return fromUserName;
    }

    public void setFromUserName(String fromUserName) {
        this.fromUserName = fromUserName;
    }

    public String getFromUserPic() {
        return fromUserPic;
    }

    public void setFromUserPic(String fromUserPic) {
        this.fromUserPic = fromUserPic;
    }
}
