package com.chatlive.data;

/**
 * Created by admin on 03/01/2017.
 */

public class SpinnerViewList {

    private int id;
    private String name;
    private String code;
    private String iso;

    public SpinnerViewList(int id, String name, String code,String iso) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.iso = iso;
    }

    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
