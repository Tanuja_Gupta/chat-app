package com.chatlive.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.chatlive.data.ChatMessage;

import java.util.ArrayList;

/**
 * Created by Intex pc on 2/2/2017.
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 6;
    // Database Name
    public static final String DATABASE_NAME = "ChattingDb";
    // Chatting table name
    private static final String TABLE_CHAT = "chat";
    private static final String TABLE_RECENTCHAT = "recentchat";
    // Chatting Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_CHATID = "chatId";
    private static final String KEY_FROMUSERID = "from_user_id";
    private static final String KEY_MESSAGE = "message";
    private static final String KEY_TOUSERDI = "destination_id";
    private static final String KEY_MESSAGETYPE = "messageType";
    private static final String KEY_CREATED_ON = "created_on";
    private static final String KEY_VIDEOTHUMB = "thumb_path";
    private static final String KEY_FROMUSERPHONE = "phone_number";
    // destination type is for checking that its group or single user
    private static final String KEY_DESTINATIONTYPE = "destination_type";
    private static final String KEY_READSTATUS = "read_status";
    private static final String KEY_MUTE = "mute";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CHAT_TABLE = "CREATE TABLE " + TABLE_CHAT + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_CHATID + " INTEGER," + KEY_FROMUSERID + " INTEGER,"
                + KEY_TOUSERDI + " INTEGER," + KEY_MESSAGE + " TEXT," + KEY_VIDEOTHUMB + " TEXT,"
                + KEY_MESSAGETYPE + " INTEGER," + KEY_CREATED_ON + " INTEGER," + KEY_DESTINATIONTYPE +
                " INTEGER," + KEY_READSTATUS + " INTEGER," + KEY_MUTE + " INTEGER" +
                ")";
        db.execSQL(CREATE_CHAT_TABLE);

        String CREATE_RECENTCHAT = "CREATE TABLE " + TABLE_RECENTCHAT + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_FROMUSERPHONE + " TEXT," + KEY_FROMUSERID + " INTEGER,"
                + KEY_DESTINATIONTYPE + " INTEGER," + KEY_MUTE + " INTEGER" +
                ")";
        db.execSQL(CREATE_RECENTCHAT);
    }


    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CHAT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RECENTCHAT);
        // Create tables again
        onCreate(db);
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    // Adding new chat
    public void addRecentChatUser(long userId, String phone, int destinationtype, int mute) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_FROMUSERID, userId);
        values.put(KEY_FROMUSERPHONE, phone);
        values.put(KEY_MUTE, mute);
        values.put(KEY_DESTINATIONTYPE, destinationtype);
        // Inserting Row
        db.insert(TABLE_RECENTCHAT, null, values);
        Log.e("UNREAD MESS", userId + "Chat added successfully  " + phone);
        db.close(); // Closing database connection
    }


    // Adding new message
    public void addMessage(int chatId, ChatMessage message) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
//        values.put(KEY_CHATID, message.getChatId());
        values.put(KEY_CHATID, chatId);
        values.put(KEY_TOUSERDI, message.getToUserId());
        values.put(KEY_MESSAGE, message.getMessage());
        values.put(KEY_VIDEOTHUMB, message.getThumb_path());
        values.put(KEY_FROMUSERID, message.getFromUserId());
        values.put(KEY_MESSAGETYPE, message.getMessageType());
        values.put(KEY_CREATED_ON, message.getCreatedOn());
        values.put(KEY_DESTINATIONTYPE, message.getDestination_type());
        values.put(KEY_READSTATUS, message.getRead_status());
        values.put(KEY_MUTE, message.getMute());
        // Inserting Row
        db.insert(TABLE_CHAT, null, values);
        Log.e("UNREAD MESS", message.getChatId() + " added successfully  " + message.getRead_status());
        db.close(); // Closing database connection
    }

    public ArrayList<ChatMessage> getChatMessage(int chatId) {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_CHAT + " WHERE " + KEY_CHATID + " =" + chatId;
        Cursor cursor = db.rawQuery(query, null);
        ArrayList<ChatMessage> contactList = new ArrayList<>();
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ChatMessage chat = new ChatMessage();
                chat.setChatId(cursor.getInt(cursor.getColumnIndex(KEY_CHATID)));
                chat.setToUserId(cursor.getInt(cursor.getColumnIndex(KEY_TOUSERDI)));
                chat.setFromUserId(cursor.getInt(cursor.getColumnIndex(KEY_FROMUSERID)));
                chat.setMessage(cursor.getString(cursor.getColumnIndex(KEY_MESSAGE)));
                chat.setMessageType(cursor.getInt(cursor.getColumnIndex(KEY_MESSAGETYPE)));
                chat.setDestination_type(cursor.getInt(cursor.getColumnIndex(KEY_DESTINATIONTYPE)));
                chat.setCreatedOn(cursor.getInt(cursor.getColumnIndex(KEY_CREATED_ON)));
                chat.setThumb_path(cursor.getString(cursor.getColumnIndex(KEY_VIDEOTHUMB)));
                chat.setMute(cursor.getInt(cursor.getColumnIndex(KEY_MUTE)));
                // Adding chat to list
                contactList.add(chat);
            } while (cursor.moveToNext());
        }

        return contactList;

    }

    public ArrayList<ChatMessage> getUnreadChat(int chatId, int readstus) {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_CHAT + " WHERE " + KEY_CHATID + " =" + chatId + " AND " + KEY_READSTATUS + " =" + readstus;
        Cursor cursor = db.rawQuery(query, null);
        ArrayList<ChatMessage> chatList = new ArrayList<>();
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ChatMessage chat = new ChatMessage();
                chat.setChatId(cursor.getInt(cursor.getColumnIndex(KEY_CHATID)));
                chat.setToUserId(cursor.getInt(cursor.getColumnIndex(KEY_TOUSERDI)));
                chat.setFromUserId(cursor.getInt(cursor.getColumnIndex(KEY_FROMUSERID)));
                chat.setMessage(cursor.getString(cursor.getColumnIndex(KEY_MESSAGE)));
                chat.setMessageType(cursor.getInt(cursor.getColumnIndex(KEY_MESSAGETYPE)));
                chat.setDestination_type(cursor.getInt(cursor.getColumnIndex(KEY_DESTINATIONTYPE)));
                chat.setCreatedOn(cursor.getInt(cursor.getColumnIndex(KEY_CREATED_ON)));
                chat.setThumb_path(cursor.getString(cursor.getColumnIndex(KEY_VIDEOTHUMB)));
                chat.setRead_status(cursor.getInt(cursor.getColumnIndex(KEY_READSTATUS)));
                // Adding chat to list
                chatList.add(chat);
            } while (cursor.moveToNext());
        }

        return chatList;


    }

    public ArrayList<ChatMessage> getAllUnreadChat(int chatId, int readstus) {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_CHAT + " WHERE " + KEY_READSTATUS + " =" + readstus + " AND " + KEY_MUTE + "=" + 0;


        Cursor cursor = db.rawQuery(query, null);
        Log.e("Query", "--" + cursor.getCount());
        ArrayList<ChatMessage> chatList = new ArrayList<>();
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ChatMessage chat = new ChatMessage();
                chat.setChatId(cursor.getInt(cursor.getColumnIndex(KEY_CHATID)));
                chat.setToUserId(cursor.getInt(cursor.getColumnIndex(KEY_TOUSERDI)));
                chat.setFromUserId(cursor.getInt(cursor.getColumnIndex(KEY_FROMUSERID)));
                chat.setMessage(cursor.getString(cursor.getColumnIndex(KEY_MESSAGE)));
                chat.setMessageType(cursor.getInt(cursor.getColumnIndex(KEY_MESSAGETYPE)));
                chat.setDestination_type(cursor.getInt(cursor.getColumnIndex(KEY_DESTINATIONTYPE)));
                chat.setCreatedOn(cursor.getInt(cursor.getColumnIndex(KEY_CREATED_ON)));
                chat.setThumb_path(cursor.getString(cursor.getColumnIndex(KEY_VIDEOTHUMB)));
                chat.setRead_status(cursor.getInt(cursor.getColumnIndex(KEY_READSTATUS)));
                chat.setMute(cursor.getInt(cursor.getColumnIndex(KEY_MUTE)));
                // Adding chat to list
                chatList.add(chat);
            } while (cursor.moveToNext());
        }

        return chatList;


    }

    // Checking for chatting in local DB
    public int chatExist(long fromUserId) {
        int chatid = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_RECENTCHAT + " WHERE " + KEY_FROMUSERID + " =" + fromUserId;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                do {
                    chatid = cursor.getInt(cursor.getColumnIndex(KEY_ID));
                }
                while (cursor.moveToNext());
            }
            return chatid;
        }

        return 0;
    }

    // Checking for chatting  mute status in local DB
    public int getMuteStatus(long chatId) {
        int mute = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_RECENTCHAT + " WHERE " + KEY_ID + " =" + chatId;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                do {
                    mute = cursor.getInt(cursor.getColumnIndex(KEY_MUTE));
                }
                while (cursor.moveToNext());
            }
            return mute;
        }
        return 0;
    }

    public boolean deleteChat(long chatId) {
        SQLiteDatabase db = this.getReadableDatabase();
        db.delete(TABLE_CHAT, KEY_CHATID + "=" + chatId, null);
        return db.delete(TABLE_RECENTCHAT, KEY_FROMUSERID + "=" + chatId, null) > 0;
    }

    public void updateChatRead(long chatId, long status) {
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_READSTATUS, 1);
        String[] args = new String[]{String.valueOf(chatId), String.valueOf(status)};
        db.update(TABLE_CHAT, values, KEY_CHATID + "=? AND " + KEY_READSTATUS + "=?", args);
        //  db.update(TABLE_CHAT,KEY_CHATID +"="+chatId+ "AND"+ KEY_READSTATUS+"="+status ,args);
    }

    public void updateMuteStatus(long chatId, int mute) {
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_MUTE, mute);
        String[] args = new String[]{String.valueOf(chatId)};
        db.update(TABLE_RECENTCHAT, values, KEY_ID + "=?", args);
        Log.e("UNREAD MESS", chatId + " updated successfully  ");
    }
}