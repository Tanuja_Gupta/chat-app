package com.chatlive.Adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.chatlive.OnLoadMoreListener;
import com.chatlive.R;
import com.chatlive.responsedata.ChatUserData;
import com.chatlive.ui.ProfileActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Intex pc on 1/31/2017.
 */

public class UserListAdapter extends RecyclerView.Adapter {
    private final int VIEW_ITEM = 1;
    private List<ChatUserData> userList;

    // The minimum amount of items to have below your current scroll position
    // before loading more.
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;
    private Context mContext;
    private AdapterView.OnItemClickListener onItemClickListener;

    public UserListAdapter(List<ChatUserData> userList, RecyclerView recyclerView, Context context, AdapterView.OnItemClickListener onItemClickListener) {
        this.userList = userList;
        this.onItemClickListener = onItemClickListener;
        mContext = context;
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();
            recyclerView
                    .addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView,
                                               int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);

                            totalItemCount = linearLayoutManager.getItemCount();
                            lastVisibleItem = linearLayoutManager
                                    .findLastVisibleItemPosition();
                            if (!loading
                                    && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                                // End has been reached
                                // Do something
                                if (onLoadMoreListener != null) {
                                    onLoadMoreListener.onLoadMore();
                                }
                                loading = true;
                            }
                        }
                    });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.lyt_useritem, parent, false);
        vh = new UserViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        String name = null;
        if (userList.get(position).getUserId().length() <= 0) {
            name = userList.get(position).getGroupId();
        }

        Picasso.with(mContext)
                .load(R.mipmap.chat_profile)
                .error(R.mipmap.chat_profile)
                .placeholder(R.mipmap.chat_profile)
                .into(((UserViewHolder) holder).img_Profile);
        if (userList.get(position).getGroupId().length() <= 0) {
            name = userList.get(position).getUserId();
        }
        ((UserViewHolder) holder).tvPhone.setText(
                userList.get(position).getName() + "--" + name);
    }

    public void setLoaded() {
        loading = false;
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public void clear() {
        userList.clear();
    }

    public void addNewList(List<ChatUserData> mList) {
        this.userList = mList;
        notifyDataSetChanged();
    }

    private class UserViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tvPhone;
        private ImageView img_Profile;

        private UserViewHolder(View v) {
            super(v);
            tvPhone = (TextView) v.findViewById(R.id.txtItem);
            img_Profile = (ImageView) v.findViewById(R.id.img_Profile);
            tvPhone.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onItemClickListener.onItemClick(null, v, getAdapterPosition(), v.getId());
        }
    }


}