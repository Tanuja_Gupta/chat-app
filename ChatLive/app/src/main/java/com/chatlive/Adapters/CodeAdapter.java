package com.chatlive.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.chatlive.R;
import com.chatlive.data.SpinnerViewList;

import java.math.BigInteger;
import java.util.List;

/**
 * Created by Intex pc on 2/1/2017.
 */
// Simple array adapter for code selection
public class CodeAdapter extends ArrayAdapter<SpinnerViewList> {
    private static final int layoutID = R.layout.code_item_list;
    private LayoutInflater layoutInflater;
    private List<SpinnerViewList> spinnerList;

    /**
     * Creates an adapter for shop selection.
     *
     * @param activity    activity context.
     * @param spinnerList list of items.
     */
    public CodeAdapter(Activity activity, List<SpinnerViewList> spinnerList) {
        super(activity, layoutID, spinnerList);
        this.layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.spinnerList = spinnerList;
    }

    public String getSpinnerCode(int position) {
        String value = spinnerList.get(position).getCode();
        return value;
    }
    public String getSpinnerISO(int position) {
        String iso = spinnerList.get(position).getIso();
        return iso;
    }

    public int getSpinnerValueID(int position) {
        int id = spinnerList.get(position).getId();
        return id;
    }

    public int getCount() {
        return spinnerList.size();
    }

    public SpinnerViewList getItem(int position) {
        return spinnerList.get(position);
    }

    public long getItemId(int position) {
        return spinnerList.get(position).getId();
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        int value = new BigInteger("e6000000", 16).intValue();
        if (parent != null)
            parent.setBackgroundColor(value);
        return getCustomView(position, convertView, parent, true);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent, false);
    }

    private View getCustomView(int position, View convertView, ViewGroup parent, boolean dropdown) {
        View v = convertView;
        ListItemHolder holder;
        if (v == null) {
            v = layoutInflater.inflate(layoutID, parent, false);
            holder = new ListItemHolder();
            holder.countrycode = (TextView) v.findViewById(R.id.tv_countryCode);
            v.setTag(holder);
        } else {
            holder = (ListItemHolder) v.getTag();
        }
        SpinnerViewList spinnerItem = spinnerList.get(position);
        if (spinnerItem != null) {
            holder.countrycode.setText(spinnerItem.getName() + "(" + spinnerItem.getCode() + ")");
        }
        return v;
    }

    static class ListItemHolder {
        TextView countrycode;
    }


}
