package com.chatlive.Adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import com.chatlive.R;
import com.chatlive.commonutils.Constants;
import com.chatlive.commonutils.SessionManager;
import com.chatlive.data.ChatMessage;
import com.chatlive.responsedata.UserData;
import com.chatlive.ui.ChatActivity;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static com.chatlive.R.id.fromVidUser;
import static com.chatlive.R.id.toVidUser;

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<ChatMessage> chatList = new ArrayList<>();
    UserData userData;
    private Context mContext;
    static AdapterView.OnItemClickListener onItemClickListener;

    public ChatAdapter(Context c, AdapterView.OnItemClickListener onItemClickListener) {
        mContext = c;
        this.onItemClickListener = onItemClickListener;
    }

    public List<ChatMessage> getChatList() {
        return chatList;
    }

    public void setChatList(List<ChatMessage> chatList) {
        this.chatList = chatList;
        notifyDataSetChanged();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        switch (viewType) {
            case 0:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_list_item_layout, parent, false);
                return new TextViewHolder(view);
            case 1:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_imagemessage, parent, false);
                return new ImageViewHolder(view);
            case 2:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_videomessage, parent, false);
                return new VideoHolder(view);
            case 3:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.groupmessage_item, parent, false);
                return new GroupTextViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        final ChatMessage chat = chatList.get(position);
        userData = SessionManager.getSavedObjectFromPreference(mContext,
                Constants.SHAREDPREFRENCE,
                "userData", UserData.class);
        if (chat != null) {
            switch (chat.getMessageType()) {
                case 0:
                    if (chat.getFromUserId() == userData.getUserId()) {
                        ((TextViewHolder) holder).txtToMessage.setVisibility(View.GONE);
                        ((TextViewHolder) holder).txtFromMessage.setVisibility(View.VISIBLE);
                        if (chat.getMessage().length() != 0) {
                            ((TextViewHolder) holder).txtFromMessage.setText(chat.getMessage());
                        }
                    } else {
                        ((TextViewHolder) holder).txtToMessage.setVisibility(View.VISIBLE);
                        ((TextViewHolder) holder).txtFromMessage.setVisibility(View.GONE);
                        if (chat.getMessage()
                                != null) {
                            if (chat.getMessage().length() != 0) {
                                ((TextViewHolder) holder).txtToMessage.setText(chat.getMessage());
                            }
                        }
                    }
                    break;
                case 1:
                    if (chat.getFromUserId() == userData.getUserId()) {
                        ((ImageViewHolder) holder).fromImgUser.setVisibility(View.VISIBLE);
                        ((ImageViewHolder) holder).toImgUser.setVisibility(View.GONE);
                        ((ImageViewHolder) holder).fromImgUser.post(new Runnable() {
                            @Override
                            public void run() {
                                Uri uri = Uri.fromFile(new File(Constants.FILEPATH + chat.getMessage().substring(chat.getMessage().lastIndexOf("/") + 1)));
                                Picasso.with(mContext)
                                        .load(uri)
                                        .error(R.mipmap.ic_launcher)
                                        .placeholder(R.mipmap.ic_launcher)
                                        .into(((ImageViewHolder) holder).fromImgUser);
                            }
                        });
                    } else {
                        ((ImageViewHolder) holder).fromImgUser.setVisibility(View.GONE);
                        ((ImageViewHolder) holder).toImgUser.setVisibility(View.VISIBLE);
                        ((ImageViewHolder) holder).toImgUser.post(new Runnable() {
                            @Override
                            public void run() {
                                Uri uri = Uri.fromFile(new File(Constants.FILEPATH + chat.getMessage().substring(chat.getMessage().lastIndexOf("/") + 1)));
                                Picasso.with(mContext)
                                        .load(uri)
                                        .error(R.mipmap.ic_launcher)
                                        .placeholder(R.mipmap.ic_launcher)
                                        .into(((ImageViewHolder) holder).toImgUser);
                            }
                        });
                    }
                    break;
                case 2:


                    if (chat.getFromUserId() == userData.getUserId()) {
                        ((VideoHolder) holder).fromVidUser.setVisibility(View.VISIBLE);
                        ((VideoHolder) holder).toVidUser.setVisibility(View.GONE);
                        ((VideoHolder) holder).fromVidUser.post(new Runnable() {
                            @Override
                            public void run() {
//                                Uri uri = Uri.fromFile(new File(Constants.FILEPATH + "thumbnail_" + chat.getThumb_path().substring(chat.getThumb_path().lastIndexOf("/") + 1)));
                                Bitmap bMap = ThumbnailUtils.createVideoThumbnail(chat.getMessage(), MediaStore.Video.Thumbnails.MICRO_KIND);
                                if (bMap != null) {
                                    ((VideoHolder) holder).fromVidUser.setImageBitmap(bMap);
                                }
                            }
                        });
                    } else {
                        ((VideoHolder) holder).fromVidUser.setVisibility(View.GONE);
                        ((VideoHolder) holder).toVidUser.setVisibility(View.VISIBLE);
                        ((VideoHolder) holder).toVidUser.post(new Runnable() {
                            @Override
                            public void run() {
                                if (chat.getThumb_path() != null) {
                                    Uri uri = Uri.fromFile(new File(Constants.FILEPATH + "thumbnail_" + chat.getThumb_path().substring(chat.getThumb_path().lastIndexOf("/") + 1)));
                                    Picasso.with(mContext)
                                            .load(uri)
                                            .error(R.mipmap.ic_launcher)
                                            .placeholder(R.mipmap.ic_launcher)
                                            .into(((VideoHolder) holder).toVidUser);
                                }
                            }
                        });
                    }
                    break;
                case 3: {
                    if (chat.getMessage().length() != 0) {
                        ((GroupTextViewHolder) holder).txtGroupText.setText(chat.getMessage());
                    }
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return chatList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (chatList != null) {
            return chatList.get(position).getMessageType();
        }
        return 0;
    }

    public static class GroupTextViewHolder extends RecyclerView.ViewHolder {
        private TextView txtGroupText;

        public GroupTextViewHolder(View itemView) {
            super(itemView);
            txtGroupText = (TextView) itemView.findViewById(R.id.txtGroupText);
        }
    }

    public static class TextViewHolder extends RecyclerView.ViewHolder {
        private TextView txtToMessage;
        private TextView txtFromMessage;

        public TextViewHolder(View itemView) {
            super(itemView);
            txtToMessage = (TextView) itemView.findViewById(R.id.txttoMessage);
            txtFromMessage = (TextView) itemView.findViewById(R.id.txtFromMessage);
        }
    }

    public static class ImageViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView fromImgUser;
        private ImageView toImgUser;

        public ImageViewHolder(View itemView) {
            super(itemView);
            fromImgUser = (ImageView) itemView.findViewById(R.id.fromImgUser);
            toImgUser = (ImageView) itemView.findViewById(R.id.toImgUser);
            fromImgUser.setOnClickListener(this);
            toImgUser.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onItemClickListener.onItemClick(null, v, getAdapterPosition(), v.getId());
        }
    }

    public static class VideoHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView fromVidUser;
        private ImageView toVidUser;

        public VideoHolder(View itemView) {
            super(itemView);
            fromVidUser = (ImageView) itemView.findViewById(R.id.fromVidUser);
            toVidUser = (ImageView) itemView.findViewById(R.id.toVidUser);
            fromVidUser.setOnClickListener(this);
            toVidUser.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onItemClickListener.onItemClick(null, v, getAdapterPosition(), v.getId());
        }
    }

}
