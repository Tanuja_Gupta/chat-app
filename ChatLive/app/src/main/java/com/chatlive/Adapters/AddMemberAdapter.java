package com.chatlive.Adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.chatlive.R;
import com.chatlive.responsedata.ChatGroupData;
import com.chatlive.responsedata.ChatUserData;
import com.chatlive.responsedata.SingleUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Intex pc on 2/10/2017.
 */

public class AddMemberAdapter extends RecyclerView.Adapter
        <AddMemberAdapter.ListItemViewHolder> {

    private Context mContext;
    private AdapterView.OnItemClickListener onItemClickListener;
    private List<ChatUserData> items;
    private SparseBooleanArray selectedItems;

    public AddMemberAdapter(List<ChatUserData> modelData, RecyclerView recyclerView, Context context, AdapterView.OnItemClickListener onItemClickListener) {
        if (modelData == null) {
            throw new IllegalArgumentException("modelData must not be null");
        }
        items = modelData;
        selectedItems = new SparseBooleanArray();
        this.onItemClickListener = onItemClickListener;
        mContext = context;
    }

    /**
     * Adds and item into the underlying data set
     * at the position passed into the method.
     *
     * @param newModelData The item to add to the data set.
     * @param position     The index of the item to remove.
     */
    public void addData(ChatUserData newModelData, int position) {
        items.add(position, newModelData);
        notifyItemInserted(position);
    }

    /**
     * Removes the item that currently is at the passed in position from the
     * underlying data set.
     *
     * @param position The index of the item to remove.
     */
    public void removeData(int position) {
        items.remove(position);
        notifyItemRemoved(position);
    }

    public ChatUserData getItem(int position) {
        return items.get(position);
    }

    @Override
    public ListItemViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.lyt_useritem, viewGroup, false);
        return new ListItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ListItemViewHolder viewHolder, int position) {
        ChatUserData model = items.get(position);
        viewHolder.tvPhone.setText(model.getPhoneNumber());
        viewHolder.itemView.setActivated(selectedItems.get(position, false));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void toggleSelection(int pos) {
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
        } else {
            selectedItems.put(pos, true);
        }
        notifyItemChanged(pos);
    }

    public void clearSelections() {
        selectedItems.clear();
        notifyDataSetChanged();
    }

    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    public List<Integer> getSelectedItems() {
        List<Integer> items = new ArrayList<Integer>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(selectedItems.keyAt(i));
        }
        return items;
    }


    public final class ListItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tvPhone;

        public ListItemViewHolder(View itemView) {
            super(itemView);
            tvPhone = (TextView) itemView.findViewById(R.id.txtItem);
            tvPhone.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onItemClickListener.onItemClick(null, v, getAdapterPosition(), v.getId());
        }
    }
}
