package com.chatlive;

import android.provider.ContactsContract;
import android.support.multidex.MultiDexApplication;

import com.chatlive.services.ApiCallService;
import com.chatlive.syncclasses.ContactsObserver;
import com.google.firebase.FirebaseApp;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Intex pc on 2/3/2017.
 */

public class ChatLiveApplication extends MultiDexApplication {
    public ApiCallService mApiCall;
    public Retrofit mRetrofit;

    @Override
    public void onCreate() {
        super.onCreate();
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .readTimeout(15L, TimeUnit.SECONDS)
                .writeTimeout(15L, TimeUnit.SECONDS)
                .build();
        mRetrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
        mApiCall = mRetrofit.create(ApiCallService.class);
        FirebaseApp.initializeApp(this);
        ContactsObserver contentObserver = new ContactsObserver(this);
        getContentResolver().registerContentObserver(ContactsContract.Contacts.CONTENT_URI, true, contentObserver);
    }

}
